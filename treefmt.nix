{ inputs, ... }:
{
  imports = [ inputs.treefmt-nix.flakeModule ];

  perSystem = _: {
    treefmt = {
      projectRootFile = "flake.lock";

      settings.formatter.nixfmt.excludes = [ "tests/testdata/*.nix" ];

      programs = {
        statix.enable = true;
        deadnix.enable = true;
        nixfmt.enable = true;
      };
    };
  };
}
