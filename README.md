# folliehiyuki's Nix configuration

This Nix flake is used to manage my NixOS and MacOS machines.

For the configuration of my other desktop hosts running either [AlpineLinux](https://alpinelinux.org) or [ChimeraLinux](https://chimera-linux.org/), see [dotfiles-ansible](/folliehiyuki/dotfiles-ansible) repository.

<!--toc:start-->
- [folliehiyuki's Nix configuration](#folliehiyukis-nix-configuration)
  - [Table of Content](#table-of-content)
  - [Setup](#setup)
    - [Darwin](#darwin)
    - [NixOS](#nixos)
  - [Credits](#credits)
<!--toc:end-->

## Setup

### Darwin

`nix` needs to be installed first: `sh <(curl -L https://nixos.org/nix/install)`

To configure a Darwin host:

```bash
nix build .#darwinConfigurations.<hostname>.system
./result/sw/bin/darwin-rebuild switch --flake .#<hostname>

# Delete the manually installed `nix` instance to make `nix config check` happy
sudo nix profile remove nix cacert
```

### NixOS

The NixOS host is set up with tmpfs on root, so a few prerequisite steps are needed before `nixos-install` can be executed:

```bash
# Destroy, partition, format the targeted disk and mount them to /mnt
# Method 1: via a custom-built disko script
nix build .#nixosConfigurations.aragorn.config.system.build.diskoScript && ./result
# Method 2: use the original `disko` script
disko --mode destroy,format,mount --flake .#aragorn

# Generate SSH key pairs
mkdir -m 0755 -p /mnt/persist/etc/ssh
ssh-keygen -t rsa -b 4096 -C root@aragorn -f /mnt/persist/etc/ssh/ssh_host_rsa_key
ssh-keygen -t ed25519 -C root@aragorn -f /mnt/persist/etc/ssh/ssh_host_ed25519_key

# Update .sops.yaml file with this new key (converted using ssh-to-age).
# Run `sops hosts/aragorn/secrets.yaml` to create the secret file.
# Run `sops updatekeys` for the host secrets if the SSH keys ever change.

# Generate a random machine-id file (should be confidential :v)
systemd-machine-id-setup --root /mnt/persist/

# Generate a fresh zpool.cache file.
# It will be symlinked later to /etc/zfs/zpool.cache using systemd-tmpfiles.
zpool set cachefile=/mnt/persist/etc/zpool.cache zroot

# Install the NixOS system
nixos-install --no-root-passwd --root /mnt --flake .#aragorn

# Unmount and reboot
umount -R /mnt
reboot

# To dry-run changes on each nixpkgs hash bump (doesn't need root privilege)
nixos-rebuild build --flake .#aragorn && nvd diff /run/current-system result
# Reconfigure the installed system in the future
nixos-rebuild switch --flake .#aragorn
```

## Credits

- [NotAShelf/nyx](https://github.com/notashelf/nyx)
- [hlissner/dotfiles](https://github.com/hlissner/dotfiles)
- [Mic92/dotfiles](https://github.com/Mic92/dotfiles)
- [lilyinstarlight/foosteros](https://github.com/lilyinstarlight/foosteros)
- <https://elis.nu/blog/2020/05/nixos-tmpfs-as-root/>
- <https://grahamc.com/blog/erase-your-darlings/>
- <https://astrid.tech/2021/12/17/0/two-disk-encrypted-zfs/>
- <https://carjorvaz.com/posts/installing-nixos-with-root-on-tmpfs-and-encrypted-zfs-on-a-netcup-vps/>
