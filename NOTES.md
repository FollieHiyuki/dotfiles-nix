# Notes for making stuff work

FIXME: move this file's content to <https://docs.folliehiyuki.com> when the site is set up properly.

## Debug Nix config

- Evaluate a final output: `nix eval --json .#nixosConfigurations.aragorn.config.nix.registry`

- Interactively check each value:

```bash
$ nix repl
Welcome to Nix 2.20.1. Type :? for help.

nix-repl> :lf .
Added 23 variables.

nix-repl> inputs.flake-parts.narHash
```

## minikube

It's better to use **vfkit** driver on MacOS:

```bash
$ minikube start --driver vfkit
```

**podman** backend also works. If rootless podman is in use, minikube needs to use **containerd** runtime:

```bash
$ minikube start --driver podman --rootless --container-runtime containerd
```

To start minikube with **qemu2** driver on MacOS:

```nix
# Add this snippet into nix-darwin configuration
{ pkgs, ... }: {
  environment = {
    systemPackages = [ pkgs.qemu_kvm ];

    # expose edk2-aarch64-code.fd in a concrete path for podman/minikube to reference
    pathsToLink = [ "/share/qemu" ];
  };

  # socket_vmnet is used to establish shared network between host and the guests
  homebrew.brews = [ "socket_vmnet" ];
}
```

```bash
# Either enable `socket_vmnet` service via HomeBrew
$ sudo brew services start socket_vmnet
# or start the process manually
$ sudo mkdir -p ${HOMEBREW_PREFIX}/var/run
$ sudo ${HOMEBREW_PREFIX}/opt/socket_vmnet/bin/socket_vmnet \
  --vmnet-gateway=192.168.105.1 \
  ${HOMEBREW_PREFIX}/var/run/socket_vmnet

$ minikube start \
  --driver qemu \
  --network socket_vmnet \
  --qemu-firmware-path /etc/profiles/per-user/${USER}/share/qemu/edk2-aarch64-code.fd
```

## podman

If podman-machine can't find qemu's UEFI firmware file, specify it in the machine configuration (see [Nixpkgs issue](https://github.com/NixOS/nixpkgs/issues/169118)):

**~/.config/containers/podman/machine/qemu/podman-machine-${PODMAN_MACHINE_INSTANCE}.json**

```json
{
  ...
  "CmdLine": [
    ...
    "file=/etc/profiles/per-user/${USER}/share/qemu/edk2-aarch64-code.fd,if=pflash,format=raw,readonly=on",
    ...
  ],
  ...
}
```

A better approach is to use `applehv` (depends on `vfkit`) or `libkrun` provider for podman-machine on MacOS. See [containers.conf(5)](https://github.com/containers/common/blob/main/docs/containers.conf.5.md).

## lima

To use lima to spawn a Docker host on MacOS:

```bash
# Start a lima VM hosting remote dockerd daemon
# (all lima templates are available at https://github.com/lima-vm/lima/tree/master/examples)
#
# qemu VM type also works, but it depends on `socket_vmnet` for the network provider
$ limactl create --vm-type=vz --network=vzNAT --name=docker template://docker

# Start the created VM
$ limactl start docker

# Add a separated Docker context pointing to the VM
$ docker context create lima-docker --docker "host=unix:///Users/${USER}/.lima/docker/sock/docker.sock"

# Switch to the new context
$ docker context use lima-docker

# docker works now \(^-^)/
$ docker images -a
```
