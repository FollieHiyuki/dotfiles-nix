_: prev: {
  ungoogled-chromium = prev.ungoogled-chromium.override {
    # ungoogled-chromium's specific flags
    # Ref: https://github.com/ungoogled-software/ungoogled-chromium/blob/master/docs/flags.md
    commandLineArgs = [
      "--disable-beforeunload"
      "--disable-search-engine-collection"
      "--extension-mime-request-handling=always-prompt-for-install" # needed for chromium-web-store extension
      "--fingerprinting-canvas-image-data-noise"
      "--fingerprinting-canvas-measuretext-noise"
      "--fingerprinting-client-rects-noise"
      # "--force-punycode-hostnames"
      "--popups-to-tabs"
      "--disable-sharing-hub"
      "--no-default-browser-check"
      "--no-pings"
      "--omnibox-autocomplete-filtering=search"

      "--enable-features=${
        builtins.concatStringsSep "," [
          "MinimalReferrers"
          "ReducedSystemInfo"
          "RemoveClientHints"
          "ClearDataOnExit"
          "DisableQRGenerator"
        ]
      }"
    ];
  };
}
