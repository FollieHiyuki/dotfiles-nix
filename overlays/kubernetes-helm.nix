final: prev: {
  kubernetes-helm = final.wrapHelm prev.kubernetes-helm {
    plugins = with final.kubernetes-helmPlugins; [
      helm-diff
      helm-unittest
      helm-mapkubeapis
    ];
  };
}
