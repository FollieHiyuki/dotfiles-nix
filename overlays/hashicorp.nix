# Disable check to save compile time.
_: prev:
prev.lib.genAttrs [
  "terraform"
  "vault"
] (name: prev.${name}.overrideAttrs { doCheck = false; })
