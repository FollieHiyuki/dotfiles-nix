{ inputs, ... }:
{
  perSystem =
    {
      inputs',
      config,
      lib,
      pkgs,
      ...
    }:
    {
      # Ref: https://nix-community.github.io/nix-unit/examples/flakes.html#flake-checks
      checks.nix-unit =
        pkgs.runCommand "nix-unit-tests" { nativeBuildInputs = [ inputs'.nix-unit.packages.nix-unit ]; }
          ''
            export HOME="$(realpath .)"
            nix-unit \
              --eval-store "$HOME" \
              --extra-experimental-features flakes \
              ${
                builtins.concatStringsSep " " (
                  lib.mapAttrsToList (name: value: "--override-input ${name} ${value}") (
                    lib.filterAttrs (name: _: name != "self" && name != "nix-unit") inputs
                  )
                )
              } \
              --flake ${inputs.self}#tests
            touch "$out"
          '';

      devShells.default = pkgs.mkShellNoCC {
        name = "nixconfig";
        meta.description = "Development shell for the Nix dotfiles";
        inputsFrom = [ config.treefmt.build.devShell ];
        packages = with pkgs; [
          inputs'.disko.packages.disko
          inputs'.nix-unit.packages.nix-unit
          inputs'.home-manager.packages.home-manager
          git
          git-lfs
          nvd
          nerdfix
          sops
          ssh-to-age
          vim
        ];
      };
    };
}
