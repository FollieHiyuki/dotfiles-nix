# TODO: potentially support FreeBSD hosts
# (either with `homeConfigurations` or https://github.com/nixos-bsd/nixbsd)
{ inputs, lib, ... }:
{
  flake = {
    darwinConfigurations =
      let
        darwinHosts = [ "hoangnguyenhuy" ];
      in
      lib.genAttrs darwinHosts (
        name:
        inputs.nix-darwin.lib.darwinSystem {
          specialArgs = {
            inherit inputs;
          };
          modules = with inputs; [
            home-manager.darwinModules.home-manager
            nix-homebrew.darwinModules.nix-homebrew
            nix-index-database.darwinModules.nix-index

            ../modules/darwin
            ../modules/home-manager
            ./${name}

            {
              system.stateVersion = 5;
              networking.computerName = name;
              networking.hostName = name;
            }
          ];
        }
      );

    nixosConfigurations =
      let
        nixosHosts = [ "aragorn" ];
      in
      lib.genAttrs nixosHosts (
        name:
        lib.nixosSystem {
          specialArgs = {
            inherit inputs;
          };
          modules = with inputs; [
            disko.nixosModules.disko
            home-manager.nixosModules.home-manager
            impermanence.nixosModules.impermanence
            # lanzaboote.nixosModules.lanzaboote
            nix-index-database.nixosModules.nix-index
            nix-virt.nixosModules.default
            sops-nix.nixosModules.sops

            ../modules/nixos
            ../modules/home-manager
            ./${name}

            { networking.hostName = name; }
          ];
        }
      );
  };
}
