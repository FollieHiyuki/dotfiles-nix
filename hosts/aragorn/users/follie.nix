{
  config,
  lib,
  pkgs,
  ...
}:
let
  defaultFirefoxProfile = config.programs.firefox.profiles.default;
in
{
  home.sessionVariables.EDITOR = "nvim";

  home.packages = with pkgs; [
    ansible
    apptainer
    caprine
    chafa
    compsize
    cosign
    croc
    curl
    element-desktop # can't install anything else due to them being marked as insecure for using libolm
    exiftool
    fd
    file
    gimp-with-plugins
    git-filter-repo
    haruna
    hexyl
    httm
    hyperfine
    imagemagick
    inkscape-with-extensions
    kdePackages.khelpcenter
    kdePackages.konversation
    keepassxc
    kopia
    krita
    kubectl
    kubectx
    kubernetes-helm
    ldns # provide drill binary
    linuxkit
    manix
    mbuffer
    minikube
    mtr
    ncdu
    ngrep
    # nh
    nix-fast-build
    nix-output-monitor
    nmap
    nurl
    nvd
    nvme-cli
    okteta
    opentofu
    p7zip
    procs
    psmisc
    (pulumi.withPackages (pulumiPackages: [
      pulumiPackages.pulumi-language-go
      pulumiPackages.pulumi-language-nodejs
    ]))
    rsync
    s5cmd
    smartmontools
    sops
    stern
    streamlink
    testdisk-qt
    tokei
    toolbox
    tor-browser
    virt-manager
    wl-clipboard-rs
    yq-go
  ];

  # Pretend that `jq` is available
  home.file."${config.xdg.binHome}/jq".source = lib.getExe pkgs.gojq;

  # Auto-start QEMU connections
  dconf.settings."org/virt-manager/virt-manager/connections" =
    lib.genAttrs
      [
        "autoconnect"
        "uris"
      ]
      (_: [
        "qemu:///system"
        "qemu:///session"
      ]);

  xdg.mimeApps = rec {
    defaultApplications =
      let
        browser = [ "firefox.desktop" ];
      in
      {
        "text/html" = browser;
        "x-scheme-handler/http" = browser;
        "x-scheme-handler/https" = browser;
        "x-scheme-handler/ftp" = browser;
        "x-scheme-handler/about" = browser;
        "x-scheme-handler/unknown" = browser;
        "application/x-extension-htm" = browser;
        "application/x-extension-html" = browser;
        "application/xhtml+xml" = browser;
        "application/x-extension-xhtml" = browser;
        "application/x-extension-xht" = browser;
        "x-scheme-handler/geo" = [ "openstreetmap-geo-handler.desktop" ];
      };
    associations.added = defaultApplications;
  };

  programs = {
    alacritty = {
      enable = true;
      settings.font.size = 13.5;
    };
    bat.enable = true;
    chromium.enable = true;
    direnv.enable = true;
    emacs.enable = true;
    firefox = {
      enable = true;
      profiles = lib.mkMerge [
        {
          # A less secure profile than the default one.
          # Use this when a website doesn't work :)
          secondary = {
            id = 1;
            settings = lib.mkMerge [
              defaultFirefoxProfile.settings
              {
                "network.http.referer.XOriginPolicy" = lib.mkForce 0;
                "webgl.disabled" = lib.mkForce false;
              }
            ];
            search = {
              default = "DuckDuckGo";
              privateDefault = "DuckDuckGo";
              force = true;
            };
          };

          github = {
            id = 2;
            settings."browser.startup.homepage" = lib.mkForce "https://github.com/notifications";
          };

          gitlab = {
            id = 3;
            settings."browser.startup.homepage" = lib.mkForce "https://gitlab.com/";
          };

          alpinelinux = {
            id = 4;
            settings."browser.startup.homepage" = lib.mkForce "https://gitlab.alpinelinux.org/alpine";
          };

          codeberg = {
            id = 5;
            settings."browser.startup.homepage" = lib.mkForce "https://codeberg.org/";
          };

          disroot = {
            id = 6;
            settings."browser.startup.homepage" = lib.mkForce "https://git.disroot.org/";
          };

          sourcehut = {
            id = 7;
            settings."browser.startup.homepage" = lib.mkForce "https://sr.ht/";
          };

          protonmail = {
            id = 8;
            settings."browser.startup.homepage" = lib.mkForce "https://mail.proton.me/";
          };

          cloudflare = {
            id = 9;
            settings."browser.startup.homepage" = lib.mkForce "https://dash.cloudflare.com/";
          };

          pixiv = {
            id = 10;
            settings = {
              "browser.startup.homepage" = lib.mkForce "https://www.pixiv.net/en/";
              "network.http.referer.XOriginPolicy" = lib.mkForce 0;
            };
          };

          mastodon = {
            id = 11;
            settings."browser.startup.homepage" = lib.mkForce "https://fosstodon.org";
          };
        }

        (lib.genAttrs
          [
            "github"
            "gitlab"
            "alpinelinux"
            "codeberg"
            "disroot"
            "sourcehut"
            "protonmail"
            "cloudflare"
            "pixiv"
            "mastodon"
          ]
          (_: {
            # Can't inherit from default profile directly, as it'll raise
            # "search.file option is read-only but is set multiple times" error.
            search = {
              inherit (defaultFirefoxProfile.search) engines;

              default = "SearxNG";
              privateDefault = "SearxNG";
              force = true;
            };

            settings = lib.mkMerge [
              defaultFirefoxProfile.settings
              {
                "privacy.clearSiteData.siteSettings" = lib.mkForce false;
                "privacy.clearHistory.siteSettings" = lib.mkForce false;
                "privacy.cpd.siteSettings" = lib.mkForce false;
              }
            ];
          })
        )
      ];
    };
    fish.enable = true;
    foot.enable = true;
    fzf.enable = true;
    gallery-dl = {
      enable = true;
      oauthEnabled.pixiv = true;
    };
    ghq.enable = true;
    git = {
      enable = true;
      delta.enable = true;
      git-privacy.enable = true;
      package = pkgs.gitFull;
      signing.key = "B0567C20730E9B11";
      userEmail = "folliekazetani@protonmail.com";
    };
    git-forgit.enable = true;
    glow.enable = true;
    go.enable = true;
    gpg.enable = true;
    k9s.enable = true;
    lsd.enable = true;
    neovim.enable = true;
    ripgrep.enable = true;
    starship.enable = true;
    thunderbird.enable = true;
    translate-shell.enable = true;
    vifm.enable = true;
    vivid.enable = true;
    wget.enable = true;
    zellij.enable = true;
    zoxide.enable = true;
  };

  services.gpg-agent.pinentryPackage = pkgs.pinentry-qt;

  services.flatpak.packages = [
    {
      appId = "org.kde.audiotube";
      origin = "flathub";
    }
  ];
}
