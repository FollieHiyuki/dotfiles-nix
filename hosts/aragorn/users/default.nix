{
  config,
  lib,
  pkgs,
  ...
}:
let
  managedNormalUsers = [
    "follie"
    "gema"
  ];
in
lib.mkMerge [
  # Set users' passwords
  (lib.foldr (a: b: lib.recursiveUpdate a b) { } (
    builtins.map (
      name:
      let
        secretPath = "hashedPassword/${name}";
      in
      {
        sops.secrets.${secretPath}.neededForUsers = true;
        users.users.${name}.hashedPasswordFile = config.sops.secrets.${secretPath}.path;
      }
    ) managedNormalUsers
  ))

  {
    # Enable corresponding shells for each user
    programs.fish.enable = true;
    programs.zsh.enable = true;

    # Let everyone use flatpak
    services.flatpak.enable = true;

    users.mutableUsers = false;
    users.users = {
      # Lock root account
      root.hashedPassword = "!";

      follie = {
        isNormalUser = true;
        description = "Hoang Nguyen";
        extraGroups = [
          "wheel"
          "networkmanager"
          "libvirtd"
          "incus-admin"
        ];
        shell = pkgs.fish;

        # Ref: https://rootlesscontaine.rs/getting-started/common/subuid/
        autoSubUidGidRange = true;
      };

      gema = {
        isNormalUser = true;
        description = "Gamer";
        extraGroups = [ "networkmanager" ];
        shell = pkgs.zsh;

        # This user doesn't run rootless containers
        autoSubUidGidRange = false;
      };
    };

    # Stuff available for all users
    environment.systemPackages = with pkgs; [
      clinfo
      glxinfo
      (hunspellWithDicts (
        with hunspellDicts;
        [
          en_US-large
          ru_RU
        ]
      ))
      libreoffice-qt6-fresh
      obs-studio
      pciutils
      qbittorrent
      usbutils
      vulkan-tools

      # To mount IOS devices
      libimobiledevice
      ifuse
    ];

    home-manager.users = lib.genAttrs managedNormalUsers (name: import ./${name}.nix);

    # The users need to be able to connect to the Nix daemon for their home-manager systemd services to work
    nix.settings.allowed-users = managedNormalUsers;
  }
]
