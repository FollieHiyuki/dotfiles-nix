{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    kdePackages.dragon
    kdePackages.kate
    unrar
  ];

  programs = {
    alacritty = {
      enable = true;
      settings.font.size = 13.5;
    };
    librewolf.enable = true;
    starship.enable = true;
    zsh.enable = true;
  };

  xdg.mimeApps = rec {
    defaultApplications."x-scheme-handler/geo" = [ "openstreetmap-geo-handler.desktop" ];
    associations.added = defaultApplications;
  };

  services.flatpak = {
    packages =
      builtins.map
        (id: {
          appId = id;
          origin = "flathub";
        })
        [
          "runtime/org.freedesktop.Platform.VulkanLayer.gamescope/x86_64/24.08"
          "com.usebottles.bottles"
          "org.kde.audiotube"
          "org.libretro.RetroArch"
          "net.rpcs3.RPCS3"
        ];

    overrides =
      let
        deviceOverride = [
          "!all"
          "shm"
          "dri"
          "input"
        ];
      in
      {
        # Not available as a libretro core yet.
        # Ref: https://github.com/libretro/RetroArch/issues/15240
        "net.rpcs3.RPCS3".Context = {
          devices = deviceOverride;
          filesystems = [
            "!home"
            "!/media"
            "!/run/media"
            "xdg-document"
          ];
        };

        "org.libretro.RetroArch".Context = {
          devices = deviceOverride;
          filesystems = [
            "!host"
            "!xdg-run/app/com.discordapp.Discord"
            "!xdg-run/gamescope-0"
            "xdg-document"
          ];
        };

        "com.usebottles.bottles".Context = {
          devices = deviceOverride;
          filesystems = [
            "xdg-download"
            "/run/media/${config.home.username}"
          ];
        };
      };
  };
}
