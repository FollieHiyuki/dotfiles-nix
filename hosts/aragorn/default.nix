{ lib, pkgs, ... }:
{
  imports = [
    ./users
    ./hardware.nix
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    defaultSopsFormat = "yaml";

    # The ssh key needs to exist (and can be used to decrypt secrets) before running `nixos-install`
    age.sshKeyPaths = [ "/persist/etc/ssh/ssh_host_ed25519_key" ];
  };

  # This setting is solely used to ensure ZFS can import pools correctly.
  # Randomly generated with `head -c4 /dev/urandom | od -A none -t x4` (ensure it's unique per network).
  networking.hostId = "b5ce5013";

  # Can't live without Internet, can we?
  networking.networkmanager.enable = true;

  # This is not a production-critical machine, so no need to look back to
  # the logs of the past 3 months.
  services.journald.extraConfig = ''
    SystemMaxUse=100M
  '';

  # Download more RAM :)
  services.zram-generator.enable = true;

  # Use an NTP client with NTS support (systemd-timesyncd is the default)
  # Currently, there are only 2 options: chrony and ntpd-rs
  services.ntpd-rs =
    let
      timeServers = [
        "nts.teambelgium.net"
        "time.cloudflare.com"
        "ntpmon.dcs1.biz"
        "nts.netnod.se"
        "virginia.time.system76.com"
        "ntp3.fau.de"
        "gps.ntp.br"
      ];
    in
    {
      enable = true;
      useNetworkingTimeServers = false;
      settings = {
        source = builtins.map (server: {
          mode = "nts";
          address = server;
        }) timeServers;
      };
    };

  # Basic protection is good
  networking.nftables.enable = true;
  networking.nftables.flushRuleset = true;
  networking.firewall = {
    enable = true;
    filterForward = true;
    pingLimit = "10/second burst 5 packets";

    # Allow DNS queries for incus and libvirt bridge interfaces
    # FIXME: https://github.com/NixOS/nixpkgs/issues/263359
    interfaces =
      lib.genAttrs
        [
          "virbr*"
          "incusbr*"
        ]
        (_: {
          allowedTCPPorts = [ 53 ];
          allowedUDPPorts = [
            53
            67
            547
          ];
        });
  };

  # Trying out the hot new sudo "rewritten in Rust"
  security.sudo-rs.enable = true;

  # Not as horrifying as SELinux, and way less effective
  # TODO: integrate some pre-built AppArmor profiles:
  # - https://gitlab.com/apparmor/apparmor-profiles
  # - https://github.com/krathalan/apparmor-profiles
  # - https://github.com/roddhjav/apparmor.d
  security.apparmor.enable = true;
  services.dbus.apparmor = "enabled";

  # DNS-over-TLS out of the box. Yay.
  services.resolved.enable = true;

  # Make sshd use host keys stored in the persisted directory
  services.openssh.hostKeys = [
    {
      path = "/persist/etc/ssh_host_ed25519_key";
      type = "ed25519";
    }
    {
      path = "/persist/etc/ssh_host_rsa_key";
      type = "rsa";
      bits = 4096;
    }
  ];

  # Rotate logs
  services.logrotate = {
    enable = true;

    # Persist the state file where it can survive reboots
    extraArgs = [
      "--state"
      "/persist/var/lib/logrotate.status"
    ];
  };

  # Keep the disks in good health
  services.zfs.autoScrub = {
    enable = true;
    interval = "monthly";
  };
  services.btrfs.autoScrub = {
    enable = true;
    interval = "monthly";
    fileSystems = [ "/media" ];
  };
  services.sanoid = {
    enable = true;
    interval = "hourly";
    templates.production = {
      autoprune = true;
      autosnap = true;
      yearly = 0;
      monthly = 1;
      weekly = 2;
      daily = 4;
      hourly = 8;
    };
    datasets."zroot/home" = {
      useTemplate = [ "production" ];
      recursive = true;
      processChildrenOnly = true;
    };
  };
  services.btrbk.instances.btrbk = {
    onCalendar = "hourly";
    settings = {
      timestamp_format = "long";
      lockfile = "/var/lib/btrbk/btrbk.lock";
      cache_dir = "/persist/var/cache/btrbk";
      btrfs_commit_delete = "yes";

      subvolume."/media" = {
        snapshot_create = "onchange";
        snapshot_preserve_min = "latest";
        snapshot_preserve = "16h 8d 4w 2m";
        snapshot_dir = "/media/.snapshots";
      };
    };
  };

  # Such a crime, using Guix on a NixOS system
  services.guix = {
    enable = true;
    nrBuildUsers = 8;
  };

  # Stupid mobile devices
  # Ref:
  # - https://wiki.nixos.org/wiki/Libimobiledevice
  # - https://nixos.wiki/wiki/Android
  services.usbmuxd.enable = true;
  services.gvfs.enable = true;

  # TODO: enable libvirt services (use modular libvirt daemons)
  # TODO: use NixVirt flake to configure default network bridge (with IPv6 and static MAC address)
  virtualisation.libvirtd = {
    enable = true;
    qemu = {
      package = pkgs.qemu_kvm;
      runAsRoot = false;
      swtpm.enable = true;
      vhostUserPackages = [ pkgs.virtiofsd ];
      ovmf.packages = [
        (pkgs.OVMF.override {
          msVarsTemplate = true;
          secureBoot = true;
          tpmSupport = true;
        }).fd
      ];
    };
  };

  # FIXME: https://github.com/NixOS/nixpkgs/pull/258379
  programs.mdevctl.enable = true;

  # systemd-oomd seems to aggressively kill processes of other user sessions
  systemd.oomd.enable = false;
  services.earlyoom = {
    enable = true;
    freeMemThreshold = 5;
    # freeMemKillThreshold = 2;
    freeSwapThreshold = 10;
    freeSwapKillThreshold = 5;
  };

  # Linux containers
  virtualisation.incus = {
    enable = true;
    socketActivation = true;
    preseed = {
      config."images.auto_update_cached" = false;
      networks = [
        {
          name = "incusbr0";
          type = "bridge";
          config = {
            "ipv4.address" = "auto";
            "ipv6.address" = "auto";
          };
        }
      ];
      profiles = [
        {
          name = "default";
          devices = {
            eth0 = {
              name = "eth0";
              network = "incusbr0";
              type = "nic";
            };
            root = {
              path = "/";
              pool = "default";
              size = "35GiB";
              type = "disk";
            };
          };
        }
      ];
      storage_pools = [
        {
          name = "default";
          driver = "dir";
        }
      ];
    };
  };
  virtualisation.lxc = {
    enable = true;
    defaultConfig = ''
      lxc.include = ${pkgs.lxcfs}/share/lxc/config/common.conf.d/00-lxcfs.conf
    '';
  };

  # Make podman systemd socket available to the user
  virtualisation.podman.enable = true;

  # TODO: enable auditd, usbguard

  # Non-English speaker here
  i18n.inputMethod.enable = true;
  i18n.inputMethod.type = "fcitx5";
  i18n.inputMethod.fcitx5.addons = [ pkgs.kdePackages.fcitx5-with-addons ];

  # Ref: https://nixos.wiki/wiki/KDE
  services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  # Most valuable directories (.e.g /home, /gnu, /nix) are persisted using ZFS datasets.
  # Impermanence is used only to deal with state files.
  # Ref: https://nixos.org/manual/nixos/stable/#ch-system-state
  environment.persistence."/persist" = {
    hideMounts = true;
    files = [
      "/etc/machine-id"
    ];
    directories = [
      "/etc/NetworkManager/system-connections"
      "/var/lib/bluetooth"
      "/var/lib/incus"
      "/var/lib/libvirt"
      "/var/lib/nixos"
      "/var/lib/systemd"
      "/var/log"
      {
        directory = "/var/tmp";
        mode = "1777";
      }
    ];
  };

  # zpool property `cachefile` isn't stored on the pool itself, so setting it to a custom
  # path doesn't work. Create a symlink to /etc/zfs/zpool.cache instead.
  # Ref: https://github.com/openzfs/zfs/blob/5945676bcc5d8b45554c93ea08a0d1f654c7075e/include/sys/fs/zfs.h#L857
  systemd.tmpfiles.rules = [
    "L /etc/zfs/zpool.cache - - - - /persist/etc/zpool.cache"
  ];

  # Ref: https://nixos.wiki/wiki/Fonts
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      sarasa-gothic
      iosevka-bin
      (iosevka-bin.override { variant = "Curly"; })
      (iosevka-bin.override { variant = "CurlySlab"; })
      (iosevka-bin.override { variant = "Slab"; })
      (iosevka-bin.override { variant = "Aile"; })
      (iosevka-bin.override { variant = "Etoile"; })
      nerd-fonts.iosevka-term
    ];
    fontconfig.defaultFonts = {
      serif = [
        "DejaVu Serif"
        "Iosevka Etoile"
        "Sarasa Fixed Slab J"
      ];
      sansSerif = [
        "DejaVu Sans"
        "Iosevka Aile"
        "Sarasa Fixed J"
      ];
      monospace = [
        "DejaVu Sans Mono"
        "Iosevka"
        "Sarasa Mono J"
      ];
    };
  };

  system.stateVersion = "24.05";
}
