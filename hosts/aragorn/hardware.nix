{ pkgs, ... }:
{
  boot.initrd.availableKernelModules = [
    "nvme"
    "xhci_pci"
    "ahci"
    "usb_storage"
    "usbhid"
    "sd_mod"
  ];
  boot.initrd.kernelModules = [ "amdgpu" ];
  boot.initrd.supportedFilesystems = [ "zfs" ];

  boot.kernelModules = [ "kvm-amd" ];
  boot.kernelParams = [ "amd_pstate=active" ];

  # INFO: toggle this option sometimes when zfs version compatibility catches up
  # (having a newer kernel is always nicer)
  boot.kernelPackages = pkgs.linuxKernel.packages.linux_xanmod;

  # Ref:
  # - https://nixos.wiki/wiki/ZFS#Tuning_other_parameters
  # - https://pve.proxmox.com/wiki/ZFS_on_Linux#sysadmin_zfs_limit_memory_usage
  # - Why shouldn't L2ARC be enabled: https://klarasystems.com/articles/openzfs-all-about-l2arc/
  # - See zfs(4) for available kernel module options for zfs
  #
  # This machine has 32GB of RAM.
  # -> the default zfs_arc_min is around 1GB (1/32 of the system RAM)
  # -> the default zfs_arc_max is 16GB (half of the system RAM)
  boot.extraModprobeConfig = ''
    options zfs zfs_arc_max=${builtins.toString (4 * 1024 * 1024 * 1024)}
    options kvm_amd nested=1
  '';

  # The default is 50%, which is too much for this machine
  boot.tmp.tmpfsSize = "25%";

  boot.loader.grub = {
    enable = true;
    mirroredBoots = [
      {
        devices = [ "nodev" ];
        path = "/boot";
      }
    ];
  };

  # Being fancy. Other people have already taken care of all the tough setting-up parts :)
  # Ref: https://wiki.nixos.org/wiki/Plymouth
  boot.plymouth = {
    enable = true;
    theme = "rings";
    themePackages = [
      (pkgs.adi1090x-plymouth-themes.override {
        selected_themes = [ "rings" ];
      })
    ];
  };

  # Necessary ~~evils~~ binary blobs
  # Ref: https://github.com/NixOS/nixos-hardware/tree/master/common/{cpu,gpu}/amd
  hardware.cpu.amd.updateMicrocode = true;

  # GPU stuff for wayland support in general
  hardware.amdgpu = {
    initrd.enable = true;
    amdvlk.enable = true;
    opencl.enable = true;
  };

  # Bluetooth is meh, but needed sometimes
  hardware.bluetooth.enable = true;

  # XP-Pen Artist 16 Pro (1st Gen)
  hardware.opentabletdriver.enable = true;

  disko.devices = {
    nodev."/" = {
      fsType = "tmpfs";
      mountOptions = [
        "defaults"
        "size=1G"
        "mode=755"
      ];
    };

    disk = {
      # I got an external HDD bay with 2 identical drives (same model number), so they
      # got duplicated in /dev/disk/by-id. Use the USB port numbers instead :(
      sda = {
        device = "/dev/disk/by-id/usb-External_USB3.0_0000007788FC-0:0";
        type = "disk";
        content = {
          type = "gpt";
          partitions.media = {
            size = "100%";
            content = {
              type = "btrfs";
              extraArgs = [
                "--force"
                "--checksum blake2" # more "secure" than the default crc32c, still relatively fast
                "--features block-group-tree" # NOTE: put here until it's the default
              ];
              subvolumes =
                let
                  btrfsMountOptions = [
                    "nofail" # this is a pluggable external drive
                    "noatime"
                    "nodev"
                    "noexec"
                    "nosuid"
                    # "autodefrag"
                    # "user_subvol_rm_allowed"
                    "rescue=usebackuproot"
                    "compress=zstd:9"
                  ];
                in
                {
                  "@root" = {
                    mountpoint = "/media";
                    mountOptions = btrfsMountOptions;
                  };
                  "@snapshots" = {
                    mountpoint = "/media/.snapshots";
                    mountOptions = btrfsMountOptions;
                  };
                };
            };
          };
        };
      };

      nvme0n1 = {
        device = "/dev/disk/by-id/nvme-eui.002538423140a12b";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "1G";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "umask=0077" ];
              };
            };
            luks = {
              size = "100%";
              content = {
                type = "luks";
                name = "crypted";
                askPassword = true;
                content = {
                  type = "zfs";
                  pool = "zroot";
                };
              };
            };
          };
        };
      };
    };

    zpool.zroot = {
      type = "zpool";
      rootFsOptions = {
        # Sensible defaults
        acltype = "posixacl";
        canmount = "off";
        compression = "zstd-9";
        dnodesize = "auto";
        normalization = "formD";
        relatime = "on";
        xattr = "sa";
        mountpoint = "none";

        # Lower write frequency
        logbias = "throughput";
        recordsize = "16K";
      };
      options = {
        ashift = "12";
        autotrim = "on";
      };
      datasets = {
        # Over-provisioning
        reserved = {
          type = "zfs_fs";
          options = {
            mountpoint = "none";

            # `refreservation` is preferred here over `reservation`, as it accounts for even snapshots. See zfsprops(7).
            # Ref: https://nex7.blogspot.com/2013/03/reservation-ref-reservation-explanation.html
            refreservation = "75G";
          };
        };
        nix = {
          type = "zfs_fs";
          mountpoint = "/nix";
          options = {
            devices = "off";
            atime = "off";
          };
        };
        gnu = {
          type = "zfs_fs";
          mountpoint = "/gnu";
          options = {
            devices = "off";
            atime = "off";
          };
        };
        persist = {
          type = "zfs_fs";
          mountpoint = "/persist";
          options = {
            setuid = "off";
            exec = "off";
            devices = "off";
          };
        };
        home = {
          type = "zfs_fs";
          options = {
            mountpoint = "/home";
            canmount = "off";
          };
        };
        "home/follie" = {
          type = "zfs_fs";
          mountpoint = "/home/follie";
        };
        "home/gema" = {
          type = "zfs_fs";
          mountpoint = "/home/gema";
        };
      };
    };
  };

  # Make the filesystem available early (disko doesn't support this option yet)
  # Ref: https://github.com/nix-community/disko/issues/192
  fileSystems."/persist".neededForBoot = true;

  nixpkgs.hostPlatform = "x86_64-linux";
}
