_: {
  home.sessionVariables.EDITOR = "nvim";

  programs = {
    alacritty = {
      enable = true;
      settings.font.size = 18;
    };
    bat.enable = true;
    direnv.enable = true;
    emacs.enable = true;
    fzf.enable = true;
    gh.enable = true;
    ghq.enable = true;
    git = {
      enable = true;
      delta.enable = true;
      signing.key = "4DDE6212A7FDB0E1";
      userEmail = "hoang.nguyenhuy@optimizely.com";
    };
    git-forgit.enable = true;
    go.enable = true;
    gpg.enable = true;
    k9s.enable = true;
    lsd.enable = true;
    neovim.enable = true;
    ripgrep.enable = true;
    ruby.enable = true;
    translate-shell.enable = true;
    starship.enable = true;
    vivid.enable = true;
    yazi.enable = true;
    zellij.enable = true;
    zoxide.enable = true;
    zsh.enable = true;
  };
}
