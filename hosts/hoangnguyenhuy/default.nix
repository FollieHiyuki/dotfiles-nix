{ pkgs, ... }:
let
  username = "hoang.nguyenhuy";
in
{
  environment.shells = [ pkgs.zsh ];

  programs = {
    amethyst.enable = true;
    zsh.enable = true;
  };

  fonts.packages = with pkgs; [
    iosevka-bin
    (iosevka-bin.override { variant = "Aile"; })
    (iosevka-bin.override { variant = "Etoile"; })
    (iosevka-bin.override { variant = "Slab"; })
    nerd-fonts.iosevka-term
  ];

  # The machine had homebrew installed before nix-homebrew was added
  nix-homebrew.autoMigrate = true;
  nix-homebrew.user = username;
  homebrew.casks = [
    "aws-vpn-client"
    "dbeaver-community"
    "displaylink"
    "headlamp"
  ];

  # Ref: https://github.com/LnL7/nix-darwin/issues/328
  # This is the primary user initially created by MacOS, so set the shell manually here.
  # (it won't be managed by nix-darwin)
  system.activationScripts.postActivation.text = ''
    chpass -s /run/current-system/sw/bin/zsh ${username}
  '';

  users.users.${username} = {
    home = "/Users/${username}";
    description = "Hoang Nguyen Huy";
    isHidden = false;
    shell = pkgs.zsh;

    packages = with pkgs; [
      age
      nixfmt-rfc-style
      aws-sso-cli
      awscli2
      azure-cli
      chafa
      croc
      cue
      docker-buildx
      docker-client
      docker-compose
      eksctl
      fd
      fluxcd
      gnumake
      go-jsonnet
      golangci-lint
      google-cloud-sdk
      hoppscotch
      (hunspellWithDicts (with hunspellDicts; [ en_US-large ]))
      keepassxc
      kubectl
      kubectx
      kubernetes-helm
      lima
      maccy
      minikube
      nodejs-slim
      opentofu
      s5cmd
      shellcheck
      sops
      ssm-session-manager-plugin
      stern
      terraform
      tokei
      vault
      vfkit
      youtube-music
      yq-go
    ];
  };

  home-manager.users.${username} = import ./user.nix;

  # Ref: https://github.com/LnL7/nix-darwin/issues/1339
  ids.gids.nixbld = 30000;

  nix.settings.trusted-users = [ username ];

  nixpkgs.hostPlatform = "aarch64-darwin";
}
