_: {
  programs.k9s = {
    settings.k9s = {
      ui = {
        skin = "nord";
        enableMouse = true;
        crumbsless = true;
        noIcons = true;
      };
      logger = {
        buffer = 2000;
        sinceSeconds = 60;
      };
      thresholds = rec {
        cpu = {
          critical = 90;
          warn = 75;
        };
        memory = cpu;
      };
    };
    skins.nord = ./nord.yml;
  };
}
