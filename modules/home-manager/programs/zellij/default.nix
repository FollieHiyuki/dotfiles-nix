_: {
  programs.zellij = {
    # Don't automatically start on a new shell session
    # These options were `false` by default, until https://github.com/nix-community/home-manager/commit/5af1b9a0f193ab6138b89a8e0af8763c21bbf491
    enableBashIntegration = false;
    enableFishIntegration = false;
    enableZshIntegration = false;

    settings = {
      pane_frames = false;
      scroll_buffer_size = 50000;
      theme = "nord";
      themes.nord = {
        fg = "#D8DEE9";
        bg = "#2E3440";
        black = "#3B4252";
        red = "#BF616A";
        green = "#A3BE8C";
        yellow = "#EBCB8B";
        blue = "#81A1C1";
        magenta = "#B48EAD";
        cyan = "#88C0D0";
        white = "#ECEFF4";
        orange = "#D08770";
      };
    };
  };
}
