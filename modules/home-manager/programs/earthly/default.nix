_: {
  programs.earthly.settings.global = {
    disable_analytics = true;
    disable_log_sharing = true;
  };
}
