{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (config) xdg;

  cfg = config.programs.luarocks;
  jsonFormat = pkgs.formats.json { };
in
with lib;
{
  options.programs.luarocks = {
    enable = mkEnableOption "luarocks";

    package = mkOption {
      type = types.package;
      default = pkgs.lua54Packages.luarocks;
      defaultText = literalExpression "pkgs.lua54Packages.luarocks";
      description = "The Luarocks package to install.";
    };

    settings = mkOption {
      inherit (jsonFormat) type;
      default = { };
      example = literalExpression ''
        {
          home_tree = "$HOME/.luarocks";
          local_by_default = false;
        }
      '';
      description = ''
        Luarocks configuration written to $XDG_CONFIG_HOME/luarocks/config.lua.
      '';
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      cfg.package
      pkgs.curl
    ];

    home.sessionVariables.LUAROCKS_CONFIG = "${config.xdg.configHome}/luarocks/config.lua";

    xdg.configFile."luarocks/config.lua".text = lib.generators.toLua {
      asBindings = true;
    } cfg.settings;

    # Default settings
    programs.luarocks.settings = {
      home_tree = "${xdg.dataHome}/luarocks";
      homeconfdir = "${xdg.configHome}/luarocks";
      local_by_default = true;
      rocks_trees = [
        {
          name = "user";
          root = "${xdg.dataHome}/luarocks";
        }
      ];
    };
  };
}
