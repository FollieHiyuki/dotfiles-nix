{
  osConfig,
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (config.home) username;

  cfg = config.programs.git;
in
with lib;
{
  options.programs.git.git-privacy.enable = mkEnableOption "git-privacy";

  config = mkMerge [
    {
      programs.git = {
        userName = mkIf (
          osConfig != null && osConfig ? users.users.${username}.description
        ) osConfig.users.users.${username}.description;
        aliases = {
          l = "log --all --graph --pretty=format:'%C(yellow)%h%Creset %C(green)%ad%Creset -%C(auto)%d %s %C(blue)<%an>%Creset' --date=format:'%Y-%m-%dT%H:%M'";
          br = "branch --sort=-committerdate --format='%(HEAD) %(color:green)%(committerdate:short)%(color:reset) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) %(color:blue)<%(authorname)>%(color:reset)'";
          s = "status -sbu";
          sbu = "submodule update --remote --rebase";
          redo = "reset HEAD~1 --mixed";
          res = "reset --hard";
          unstage = "reset HEAD";
          wdiff = "diff --word-diff";
          compare = "!git log $1...$2 --format='%C(bold)%C(green)%m%Creset %C(yellow)%h%Creset %C(bold)<%an>%Creset - %C(blue)%s%Creset' --left-right";
          pfwl = "push --force-with-lease";

          # Ref: https://www.youtube.com/watch?v=3IIaOj1Lhb0
          stsh = "stash --keep-index";
          staash = "stash --include-untracked";
          staaash = "stash --all";
          standup = "log --all --author='${cfg.userName}' --since='9am yesterday' --format=%s";
        };
        lfs = {
          enable = true;
          skipSmudge = true;
        };
        signing.signByDefault = true;
        extraConfig = {
          core = {
            whitespace = "trailing-space";
            autocrlf = "input";
          };
          diff = {
            wsErrorHighlight = "all";
            colorMoved = "default";
          };
          branch.sort = "-committerdate";
          init.defaultBranch = "main";
          commit.verbose = true;
          rebase.autoSquash = true;
          pull.rebase = true;
          push.followTags = true;
          fetch.prune = true;
          submodule.recurse = true;
          help.autoCorrect = "prompt";
          format.useAutoBase = true;

          # External stuff
          sendemail.annotate = true;
        };
      };
    }

    (mkIf (!cfg.delta.enable) {
      programs.git.extraConfig.color.diff = {
        meta = "blue";
        frag = "yellow";
        old = "red";
        new = "green";
        oldMoved = "magenta";
        newMoved = "cyan";
        whitespace = "red reverse";
      };
    })

    (mkIf cfg.delta.enable {
      home.sessionVariables.DELTA_PAGER = "${pkgs.less}/bin/less -S";

      programs.git = {
        extraConfig.merge.conflictStyle = "zdiff3";

        delta.options = {
          navigate = true;
          hyperlinks = true;
          line-numbers = true;
          syntax-theme = "Nord";
          file-added-label = "[+]";
          file-copied-label = "[=]";
          file-modified-label = "[*]";
          file-removed-label = "[-]";
          file-renamed-label = "[>]";
          file-decoration-style = "none";
          file-style = "omit";
          hunk-header-decoration-style = "blue";
          hunk-header-file-style = "blue bold";
          hunk-header-line-number-style = "yellow box bold";
          hunk-header-style = "file line-number syntax bold italic";
          plus-style = "syntax #45504F";
          plus-emph-style = "syntax #697966";
          minus-style = "syntax #4B3D48";
          minus-emph-style = "syntax #774B55";
          line-numbers-minus-style = "brightred";
          line-numbers-plus-style = "brightgreen";
          line-numbers-left-style = "blue";
          line-numbers-right-style = "blue";
          line-numbers-zero-style = "white";
          whitespace-error-style = "#7B88A1";
          map-styles = builtins.concatStringsSep ", " [
            "bold purple => syntax #494656"
            "bold blue => syntax #384356"
            "bold cyan => syntax #40505D"
            "bold yellow => syntax #54524F"
          ];
          zero-style = "syntax";
          blame-code-style = "syntax";
          blame-format = "{author:<18} {commit:<6} {timestamp:<15}";
          blame-palette = "#2E3440 #3B4252 #434C5E";
          merge-conflict-begin-symbol = "~";
          merge-conflict-end-symbol = "~";
          merge-conflict-ours-diff-header-style = "yellow bold";
          merge-conflict-ours-diff-header-decoration-style = "#5E81AC box";
          merge-conflict-theirs-diff-header-style = "yellow bold";
          merge-conflict-theirs-diff-header-decoration-style = "#5E81AC box";
        };
      };
    })

    (mkIf cfg.git-privacy.enable {
      home.packages = [ pkgs.git-privacy ];

      programs.git.extraConfig.privacy = {
        ignoreTimezone = true;
        pattern = "hms";
      };
    })
  ];
}
