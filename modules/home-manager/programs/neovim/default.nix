{
  config,
  lib,
  pkgs,
  ...
}:
let
  gitBin = "${config.programs.git.package}/bin/git";
in
{
  programs.neovim = {
    withPython3 = true;
    withNodeJs = true;

    # to compile tree-sitter grammars
    extraPackages = with pkgs; [
      gcc
      tree-sitter
    ];
  };

  # Clone my personal Neovim config
  home.activation.fetchNeovimConfig = lib.mkIf config.programs.neovim.enable ''
    nvim_confdir="${config.xdg.configHome}/nvim"
    if [ -d "$nvim_confdir" ]; then
      cd "$nvim_confdir"
      ${gitBin} pull --rebase origin main || true
    else
      ${gitBin} clone --depth=1 --branch=main https://gitlab.com/folliehiyuki/nvim.git "$nvim_confdir"
    fi
  '';
}
