{
  osConfig,
  pkgs,
  lib,
  ...
}:
lib.mkIf (pkgs.stdenv.isDarwin && osConfig.programs.amethyst.enable) {
  xdg.configFile."amethyst/amethyst.yml".source = ./amethyst.yml;
}
