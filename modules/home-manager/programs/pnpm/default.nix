{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.programs.pnpm.enable = lib.mkEnableOption "pnpm";

  config = lib.mkIf config.programs.pnpm.enable {
    home.packages = [ pkgs.pnpm ];

    home.sessionPath = [ "${config.xdg.dataHome}/pnpm" ];
  };
}
