{ config, lib, ... }:
{
  home.sessionVariables = lib.mkIf config.programs.zoxide.enable {
    # _ZO_ECHO = 1;
    _ZO_RESOLVE_SYMLINKS = 1;
    _ZO_FZF_OPTS = "${config.home.sessionVariables.FZF_DEFAULT_OPTS} --no-multi";
  };
}
