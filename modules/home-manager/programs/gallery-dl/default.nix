{ config, lib, ... }:
let
  inherit (config) xdg home;
in
{
  # Encrypted configuration for gallery-dl provided by `subconfigs` key and sops-nix
  imports = [
    ./pixiv.nix
  ];

  # yt-dlp is needed for ugoira, or Twitter videos
  programs.yt-dlp.enable = lib.mkDefault config.programs.gallery-dl.enable;

  programs.gallery-dl.settings = rec {
    extractor = {
      archive = "${xdg.dataHome}/gallery-dl/archive.sqlite3";
      base-directory = lib.mkDefault "${home.homeDirectory}/Pictures/gallery-dl";
      image-unique = true;
      chapter-unique = true;
      date-format = "%Y-%m-%dT%H:%M:%S";
      user-agent = home.userAgentString;

      mastodon = {
        filename = "{id}_{media[id]}.{extension}";
        directory = [
          "mastodon"
          "{account[acct]!l}"
        ];
        # reblogs = true;

        "fosstodon.org" = {
          root = "https://fosstodon.org";
        };
      };

      nitter = {
        filename = "{tweet_id}_{num}.{extension}";
        directory = [
          "twitter"
          "{user[name]}"
        ];
        inherit (extractor.twitter) quoted;
        inherit (extractor.twitter) videos;

        "nitter.absturztau.be" = {
          root = "https://nitter.absturztau.be";
        };
      };

      danbooru = {
        external = true;
        ugoira = true;
      };

      artstation.directory = [
        "ArtStation"
        "{user[username]}"
      ];

      deviantart = {
        directory = [
          "DevianArt"
          "{author[username]}"
        ];
        include = [
          "gallery"
          "scraps"
          "favorite"
          "avatar"
        ];
        journals = "text";
      };

      mangadex = {
        lang = "en";
        directory = [
          "Manga"
          "{manga}"
          "c{chapter}{chapter_minor} - {title}"
        ];
      };

      mangafox = {
        chapter-filter = "lang == 'en'";
        directory = [
          "Manga"
          "{manga}"
          "c{chapter}{chapter_minor}"
        ];
      };

      mangahere = {
        inherit (extractor.mangafox) chapter-filter;
        inherit (extractor.mangadex) directory;
      };

      mangakakalot = extractor.mangahere;

      mangaread = extractor.mangafox;

      mangasee = extractor.mangafox;

      twitter = {
        videos = "ytdl";
        cards = extractor.twitter.videos;
        cards-blacklist = [
          "summary"
          "youtube.com"
          "player:twitch.tv"
        ];
        include = [
          "avatar"
          "background"
          "timeline"
          "media"
        ];
        quoted = true;
        pinned = true;
        retweets = "original";
        twitpic = true;
        # csrf = "auto";
      };
    };
    postprocessors = [
      {
        name = "zip";
        extension = "cbz";
        mode = "safe";
        whitelist = [
          "mangadex"
          "mangafox"
          "mangahere"
          "mangakakalot"
          "mangaread"
          "mangasee"
        ];
      }
    ];
    downloader = {
      part-directory = "/tmp/gallery-dl/.download";
      ytdl = {
        format = "bestvideo+bestaudio/best";
        config-file = "${xdg.configHome}/yt-dlp/config";
      };
    };
    output = {
      shorten = "eaw";
      logfile = {
        path = "${xdg.stateHome}/gallery-dl/gallery-dl.log";
        mode = "a";
        level = "debug";
        format = "{asctime} {name} [{levelname}]: {message}";
      };
      unsupportedfile = {
        path = "${xdg.stateHome}/gallery-dl/unsupported.txt";
        mode = "a";
        format = "{asctime} {message}";
        format-date = "%Y-%m-%d-%H-%M-%S";
      };
    };
    cache.file = "${xdg.cacheHome}/gallery-dl/cache.sqlite3";
  };
}
