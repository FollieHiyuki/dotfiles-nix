{ config, lib, ... }:
let
  secretPath = "gallery-dl/pixiv-refresh-token";

  templateFile = "gallery-dl/pixiv.json";
in
with lib;
{
  options.programs.gallery-dl.oauthEnabled.pixiv = mkOption {
    type = types.bool;
    default = false;
    description = ''
      Whether to include Pixiv configuration for gallery-dl,
      with refresh-token provided by sops-nix.
    '';
  };

  config = lib.mkIf config.programs.gallery-dl.oauthEnabled.pixiv {
    sops = {
      secrets.${secretPath} = { };

      # Pixiv requires authentication, so define the whole configuration
      # block for it here instead of in the main configuration file
      templates.${templateFile}.content = builtins.toJSON {
        extractor.pixiv = {
          refresh-token = config.sops.placeholder.${secretPath};
          filename = "{id}_p{num}.{extension}";
          directory = [
            "Pixiv"
            "{user[name]}-{user[id]}"
          ];
          include = [
            "background"
            "artworks"
            "avatar"
          ];
          tags = "original";
        };
      };
    };

    programs.gallery-dl.settings.subconfigs = [
      config.sops.templates.${templateFile}.path
    ];
  };
}
