{
  config,
  pkgs,
  lib,
  ...
}:
with lib;
{
  options.programs.glow.enable = mkEnableOption "glow";

  config = mkIf config.programs.glow.enable {
    home.packages = [ pkgs.glow ];

    xdg.configFile."glow/glow.yml".text = builtins.toJSON {
      style = ./nord.json;
      local = true;
      mouse = true;
      pager = false;
      width = 100;
    };
  };
}
