{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
{
  options.programs.vivid.enable = mkEnableOption "vivid";

  config = mkIf config.programs.vivid.enable {
    home.packages = [ pkgs.vivid ];

    # Set LS_COLORS for each shell at login
    programs.fish.shellInit = ''set -gx LS_COLORS (vivid generate nord)'';
    programs.zsh.initExtraBeforeCompInit = ''export LS_COLORS="$(vivid generate nord)"'';
  };
}
