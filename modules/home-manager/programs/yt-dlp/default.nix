{ config, lib, ... }:
{
  programs.yt-dlp.settings = {
    xff = "default";
    add-headers = "User-Agent:\"${config.home.userAgentString}\"";
    output = lib.mkDefault "${config.home.homeDirectory}/Downloads/ytdl/%(title)s.%(ext)s";
  };
}
