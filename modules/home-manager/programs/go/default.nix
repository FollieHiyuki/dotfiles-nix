{ lib, config, ... }:
lib.mkIf config.programs.go.enable {
  home.sessionPath = [ "${config.home.sessionVariables.GOPATH}/bin" ];

  programs.go.telemetry = {
    mode = "off";
    date = "2025-01-01";
  };
}
