{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (config) xdg;

  shellAbbrs = lib.filterAttrs (
    _: val: builtins.typeOf val == "string"
  ) config.programs.fish.shellAbbrs;
in
with lib;
{
  # Set up abbreviations via the user's configuration file
  xdg.configFile."zsh-abbr/user-abbreviations" = {
    inherit (config.programs.zsh) enable;

    text = ''
      ${concatLines (
        mapAttrsToList (key: val: "abbr --quiet ${escapeShellArg key}=${escapeShellArg val}") shellAbbrs
      )}
    '';
  };

  programs.zsh = {
    dotDir = ".config/zsh";
    defaultKeymap = "viins";

    # These variables needs to be set before sourcing plugins
    initExtraFirst = ''
      # Load all the settings immediately on sourcing, so
      # they can be overridden later by other plugins
      export ZVM_INIT_MODE=sourcing
      export ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT

      export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
      export ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=30
    '';

    # Ref: zshcompsys(1)
    # - https://github.com/hlissner/dotfiles/blob/master/config/zsh/completion.zsh
    # - https://thevaluable.dev/zsh-completion-guide-examples/
    completionInit = ''
      autoload -Uz compinit
      zmodload zsh/complist
      compinit -d ${escapeShellArg xdg.cacheHome}/zsh/zcompdump-"$ZSH_VERSION"

      # General completion menu config
      zstyle ':completion:*' menu select
      zstyle ':completion:*' group-name '''
      zstyle ':completion:*' rehash true
      zstyle ':completion:*' special-dirs true
      zstyle ':completion:*' verbose true

      # Cache completion list
      zstyle ':completion::complete:*' use-cache true
      zstyle ':completion::complete:*' cache-path ${escapeShellArg xdg.cacheHome}/zsh/zcompcache

      # Fuzzy matching
      zstyle ':completion:*' completer _complete _match _approximate
      zstyle ':completion:*:match:*' original only
      zstyle ':completion:*:approximate:*' max-errors 1 numeric

      # Use LS_COLORS for completion list
      # (LS_COLORS should be set before this is run)
      zstyle ':completion:*:default' list-colors ''${(s.:.)LS_COLORS}
      # Make color
      zstyle ':completion:*:corrections' format '%B-- %F{green}%d%f (errors: %F{red}%e%f) --%b'
      zstyle ':completion:*:descriptions' format '%B%F{cyan}[%d]%f%b'
      zstyle ':completion:*:messages' format '%F{yellow}-- %d --%f'
      zstyle ':completion:*:warnings' format '%B%F{red}-- No match for: %d --%f%b'

      # Also complete options for cd, chdir, pushd
      zstyle ':completion:*' complete-options true
      # Shorten directories' names when cd-ing
      zstyle ':completion:*:paths' path-completion true
      # Case insensitive
      zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|=*' 'l:|=* r:|=*'
      # Expand slashes the UNIX way
      zstyle ':completion:*' squeeze-slashes true
      # Make the completion list scrollable
      zstyle ':completion:*:default' list-prompt '%S%M matches%s'
      # Don't complete unavailable commands.
      zstyle ':completion:*:functions' ignored-patterns '(_*|pre(cmd|exec))'
      # Array completion element sorting.
      zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters
    '';

    initExtra = ''
      # Set zsh options. See zshoptions(1)
      unsetopt ALWAYS_TO_END
      setopt AUTO_CD
      setopt AUTO_LIST
      setopt AUTO_MENU
      unsetopt AUTO_NAME_DIRS
      setopt AUTO_PUSHD
      unsetopt AUTO_REMOVE_SLASH
      unsetopt BEEP
      unsetopt BG_NICE
      unsetopt CHECK_JOBS
      unsetopt COMPLETE_ALIASES
      setopt COMPLETE_IN_WORD
      setopt CORRECT
      unsetopt CORRECT_ALL
      unsetopt GLOB_DOTS
      setopt HASH_LIST_ALL
      setopt HIST_BEEP
      setopt HIST_FIND_NO_DUPS
      setopt HIST_REDUCE_BLANKS
      setopt HIST_VERIFY
      unsetopt HUP
      setopt INTERACTIVE_COMMENTS
      setopt LONG_LIST_JOBS
      unsetopt MENU_COMPLETE
      unsetopt NOMATCH
      setopt NOTIFY
      setopt PUSHD_IGNORE_DUPS
      setopt PUSHD_SILENT
      setopt PUSHD_TO_HOME

      # Ensure these binary modules are available. See zshmodules(1)
      zmodload zsh/attr zsh/zle zsh/zpty

      # Load nice addons. See zshcontrib(1)
      autoload -Uz edit-command-line zcalc zmv zargs
      zle -N edit-command-line

      # Enable aliases of color names to corresponding escape sequences
      autoload -Uz colors && colors

      # zsh-autosuggestions
      bindkey -M viins '^F' autosuggest-accept

      # zsh-history-substring-search
      bindkey '^[[A' history-substring-search-up
      bindkey '^[[B' history-substring-search-down
      bindkey -M viins '^P' history-substring-search-up
      bindkey -M viins '^N' history-substring-search-down
      bindkey -M vicmd 'k' history-substring-search-up
      bindkey -M vicmd 'j' history-substring-search-down

      # Navigate the completion menu
      bindkey -M menuselect '^[[Z' reverse-menu-complete
      bindkey -M menuselect '^H' vi-backward-char
      bindkey -M menuselect '^J' vi-down-line-or-history
      bindkey -M menuselect '^K' vi-up-line-or-history
      bindkey -M menuselect '^L' vi-forward-char

      # Custom key bindings
      bindkey '^[e' edit-command-line
      bindkey -M viins '^A' beginning-of-line
      bindkey -M viins '^E' end-of-line

      # Cute bunnies are important
      ${escapeShellArg xdg.binHome}/bunny.sh
    '';

    history = {
      path = "${xdg.dataHome}/zsh/zsh_history";
      size = 12000;
      save = 10000;
      extended = true;
      expireDuplicatesFirst = true;
      ignoreDups = false;
      ignorePatterns = [
        "rm *"
        "pkill *"
        "kill *"
      ];
    };

    plugins = [
      {
        name = "zsh-vi-mode";
        src = pkgs.fetchFromGitHub {
          owner = "jeffreytse";
          repo = "zsh-vi-mode";
          rev = "cd730cd347dcc0d8ce1697f67714a90f07da26ed";
          hash = "sha256-UQo9shimLaLp68U3EcsjcxokJHOTGhOjDw4XDx6ggF4=";
        };
      }
      {
        name = "fast-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
          owner = "zdharma-continuum";
          repo = "fast-syntax-highlighting";
          rev = "cf318e06a9b7c9f2219d78f41b46fa6e06011fd9";
          hash = "sha256-RVX9ZSzjBW3LpFs2W86lKI6vtcvDWP6EPxzeTcRZua4=";
        };
      }
      {
        name = "zsh-completions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-completions";
          rev = "c160d09fddd28ceb3af5cf80e9253af80e450d96";
          hash = "sha256-EG6bwbwZW9Cya/BGZ83J5YEAUdfJ0UAQC7bRG7cFL2k=";
        };
      }
      {
        name = "zsh-history-substring-search";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-history-substring-search";
          rev = "87ce96b1862928d84b1afe7c173316614b30e301";
          hash = "sha256-1+w0AeVJtu1EK5iNVwk3loenFuIyVlQmlw8TWliHZGI=";
        };
      }
      {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-autosuggestions";
          rev = "0e810e5afa27acbd074398eefbe28d13005dbc15";
          hash = "sha256-85aw9OM2pQPsWklXjuNOzp9El1MsNb+cIiZQVHUzBnk=";
        };
      }
      {
        name = "zsh-autopair";
        src = pkgs.fetchFromGitHub {
          owner = "hlissner";
          repo = "zsh-autopair";
          rev = "449a7c3d095bc8f3d78cf37b9549f8bb4c383f3d";
          hash = "sha256-3zvOgIi+q7+sTXrT+r/4v98qjeiEL4Wh64rxBYnwJvQ=";
        };
      }
      {
        name = "zsh-abbr";
        src = pkgs.fetchFromGitHub {
          owner = "olets";
          repo = "zsh-abbr";
          rev = "716532a44e3d1548dd613da6a7824af23cdd42eb";
          hash = "sha256-PWr8o0so2ZfQJkinkLRa4bFxZtw0Lgs7UXVWvd/rWF0=";
          fetchSubmodules = true;
        };
      }
    ];
  };
}
