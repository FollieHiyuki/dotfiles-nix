{ config, lib, ... }:
let
  cfg = config.programs.git;

  gpgCfg = config.programs.gpg;
in
{
  programs.gpg = {
    homedir = "${config.xdg.dataHome}/gnupg";
    settings = {
      default-key = lib.mkIf (cfg.signing != null && cfg.signing.key != null) cfg.signing.key;
      auto-key-locate = "keyserver";
      keyserver-options = "no-honor-keyserver-url";
      cert-digest-algo = "SHA384";
      s2k-cipher-algo = "AES256";
      s2k-digest-algo = "SHA384";
      personal-cipher-preferences = "AES256 CAMELLIA256 TWOFISH";
      personal-digest-preferences = "SHA384 SHA512 SHA256";
      personal-compress-preferences = "ZLIB BZIP2 ZIP Uncompressed";
      default-preference-list = "SHA384 SHA512 SHA256 AES256 CAMELLIA256 TWOFISH ZLIB BZIP2 ZIP Uncompressed";
      default-new-key-algo = "ed25519/cert,sign+cv25519/encr";

      display-charset = "utf-8";
      no-comments = true;
      no-greeting = true;
      no-emit-version = true;
      ignore-time-conflict = true;
      no-symkey-cache = true;
      with-fingerprint = true;
      with-keygrip = true;
      keyid-format = "0xlong";
      default-recipient-self = true;
      require-cross-certification = true;
    };
  };

  home.file."${gpgCfg.homedir}/dirmngr.conf" = {
    inherit (gpgCfg) enable;
    text = ''
      keyserver hkps://keys.gnupg.net
    '';
  };
}
