{ config, ... }:
{
  programs.firefox = {
    # Ref: https://mozilla.github.io/policy-templates/
    # `EnableTrackingProtection` is not needed, as browser.contentblocking.category is set to `strict` below.
    policies = {
      AutofillAddressEnabled = false;
      AutofillCreditCardEnabled = false;
      ContentAnalysis = {
        Enabled = false;
        DefaultResult = 0;
      };
      DisplayBookmarksToolbar = "always";
      DisplayMenuBar = "never";
      SearchBar = "unified";
      HttpsOnlyMode = "force_enabled";
      ShowHomeButton = true;
      UseSystemPrintDialog = true;
      CaptivePortal = false;
      Cookies = {
        Behavior = "reject-tracker-and-partition-foreign";
        BehaviorPrivateBrowsing = "reject-tracker-and-partition-foreign";
        Locked = true;
      };
      DefaultDownloadDirectory = "${config.home.homeDirectory}/Downloads";
      NewTabPage = false;
      DisableAppUpdate = true;
      DisableFeedbackCommands = true;
      DisableFirefoxAccounts = true;
      DisableFirefoxStudies = true;
      DisableFormHistory = true;
      DisablePocket = true;
      DisableSetDesktopBackground = true;
      DisableTelemetry = true;
      DontCheckDefaultBrowser = true;
      GoToIntranetSiteForSingleWordEntryInAddressBar = true;
      FirefoxHome = {
        Search = true;
        TopSites = false;
        SponsoredTopSites = false;
        Highlights = false;
        Pocket = false;
        SponsoredPocket = false;
        Snippets = false;
        Locked = true;
      };
      FirefoxSuggest = {
        WebSuggestions = false;
        SponsoredSuggestions = false;
        ImproveSuggest = false;
        Locked = true;
      };
      SearchSuggestEnabled = false;
      HardwareAcceleration = true;
      NetworkPrediction = false;
      NoDefaultBookmarks = true;
      OfferToSaveLogins = false;
      OverrideFirstRunPage = "";
      DisableMasterPasswordCreation = true;
      PasswordManagerEnabled = false;
      PopupBlocking = {
        Default = true;
        Locked = true;
      };
      PromptForDownloadLocation = true;
      Proxy = {
        Mode = "system";
        UseProxyForDNS = true;
        AutoLogin = false;
        Locked = true;
      };
      SanitizeOnShutdown = true;
      UserMessaging = {
        ExtensionRecommendations = false;
        FeatureRecommendations = false;
        SkipOnboarding = true;
        MoreFromMozilla = false;
        Locked = true;
      };
      DNSOverHTTPS = {
        Enabled = false;
        Locked = true;
      };
      Permissions = {
        Camera.BlockNewRequests = true;
        Microphone.BlockNewRequests = true;
        Location = {
          BlockNewRequests = true;
          Locked = true;
        };
        Notifications = {
          BlockNewRequests = true;
          Locked = true;
        };
        Autoplay = {
          Default = "block-audio-video";
          Locked = true;
        };
        VirtualReality = {
          BlockNewRequests = true;
          Locked = true;
        };
      };

      # Common extensions to always install
      # Ref: https://github.com/arkenfox/user.js/wiki/4.1-Extensions
      ExtensionUpdate = true;
      ExtensionSettings =
        builtins.mapAttrs
          (_: val: {
            installation_mode = "force_installed";
            install_url = "https://addons.mozilla.org/firefox/downloads/latest/${val}/latest.xpi";
          })
          {
            "uBlock0@raymondhill.net" = "ublock-origin";
            "7esoorv3@alefvanoon.anonaddy.me" = "libredirect";
            "skipredirect@sblask" = "skip-redirect";
            "@testpilot-containers" = "multi-account-containers";
            "{74145f27-f039-47ce-a470-a662b129930a}" = "clearurls";
          };

      # Ref: https://github.com/arkenfox/user.js/blob/master/user.js
      Preferences =
        builtins.mapAttrs
          (_: val: {
            Value = val;
            Status = "locked";
          })
          {
            "browser.contentblocking.category" = "strict";
            "browser.display.use_system_colors" = false;
            "browser.download.manager.addToRecentDocs" = false;
            "browser.download.always_ask_before_handling_new_types" = true;
            "browser.helperApps.deleteTempFileOnExit" = true;
            "browser.link.open_newwindow" = 3;
            "browser.link.open_newwindow.restriction" = 0;
            "browser.places.speculativeConnect.enabled" = false;
            "browser.send_pings" = false;
            "browser.sessionstore.privacy_level" = 2;
            "browser.sessionstore.resume_from_crash" = false;
            "browser.startup.page" = 1;
            "browser.startup.homepage_override.mstone" = "ignore";
            "browser.tabs.searchclipboardfor.middleclick" = false;
            "browser.uitour.enabled" = false;
            "browser.urlbar.addons.featureGate" = false;
            "browser.urlbar.mdn.featureGate" = false;
            "browser.urlbar.trending.featureGate" = false;
            "browser.urlbar.pocket.featureGate" = false;
            "browser.urlbar.weather.featureGate" = false;
            "browser.urlbar.yelp.featureGate" = false;
            "browser.urlbar.clipboard.featureGate" = false;
            "browser.urlbar.fakespot.featureGate" = false;
            "browser.urlbar.quicksuggest.enabled" = false;
            "browser.urlbar.speculativeConnect.enabled" = false;
            "browser.search.suggest.enabled.private" = false;
            "browser.search.separatePrivateDefault" = true;
            "browser.search.separatePrivateDefault.ui.enabled" = true;
            "browser.shell.shortcutFavicons" = false;
            "browser.translations.automaticallyPopup" = false;
            "browser.xul.error_pages.expert_bad_cert" = true;
            "dom.security.https_only_mode_pbm" = true;
            "dom.security.https_only_mode_ever_enabled" = true;
            "dom.security.https_only_mode_ever_enabled_pbm" = true;
            "dom.security.https_only_mode_send_http_background_request" = false;
            "dom.disable_window_move_resize" = true;
            "dom.private-attribution.submission.enabled" = false;
            "media.autoplay.blocking_policy" = 0;
            "media.autoplay.default" = 5;
            "media.gmp-provider.enabled" = false;
            "media.peerconnection.ice.default_address_only" = true;
            "media.peerconnection.ice.proxy_only_if_behind_proxy" = true;
            "network.IDN_show_punycode" = true;
            "network.auth.subresource-http-auth-allow" = 1;
            "network.connectivity-service.enabled" = false;
            "network.file.disable_unc_paths" = true;
            "network.gio.supported-protocols" = "";
            "network.http.referer.XOriginTrimmingPolicy" = 2;
            "network.http.speculative-parallel-limit" = 0;
            "network.predictor.enable-prefetch" = false;
            "network.predictor.enabled" = false;
            "network.prefetch-next" = false;
            "pdfjs.enableScripting" = false;
            "security.OCSP.enabled" = 1;
            "security.OCSP.require" = true;
            "security.insecure_connection_text.enabled" = true;
            "security.ssl.errorReporting.enabled" = false;
            "security.ssl.require_safe_negotiation" = true;
            "security.tls.enable_0rtt_data" = false;
            "signon.autofillForms" = false;
            "signon.formlessCapture.enabled" = false;

            # No crash reports
            "browser.tabs.crashReporting.sendReport" = false;
            "browser.crashReports.unsubmittedCheck.enabled" = false;
            "browser.crashReports.unsubmittedCheck.autoSubmit2" = false;

            # No addon recommendations. I know what I want to install.
            "extensions.getAddons.showPane" = false;
            "extensions.htmlaboutaddons.recommendations.enabled" = false;

            # disable shopping experience
            "browser.shopping.experience2023.enabled" = false;

            # Disable Google Safe Browing
            "browser.safebrowsing.blockedURIs.enabled" = false;
            "browser.safebrowsing.downloads.enabled" = false;
            "browser.safebrowsing.malware.enabled" = false;
            "browser.safebrowsing.phishing.enabled" = false;

            # Use Mozilla's geolocation service
            "geo.provider.ms-windows-location" = false;
            "geo.provider.use_corelocation" = false;
            "geo.provider.use_geoclue" = false;

            # Force the usage of Container Tabs
            "privacy.globalprivacycontrol.enabled" = true;
            "privacy.userContext.enabled" = true;
            "privacy.userContext.ui.enabled" = true;

            # Always show window title bar
            "browser.tabs.inTitlebar" = 0;
          };
    };

    # Create a default profile for all deployments as a reference
    profiles.default = {
      id = 0;

      containersForce = true;
      containers = {
        Personal = {
          id = 1;
          color = "blue";
          icon = "fingerprint";
        };
        Work = {
          id = 2;
          color = "yellow";
          icon = "briefcase";
        };
        Shopping = {
          id = 3;
          color = "purple";
          icon = "cart";
        };
        Other = {
          id = 4;
          color = "green";
          icon = "pet";
        };
      };

      search = {
        default = "SearxNG";
        privateDefault = "SearxNG";
        engines = import ./search-engines.nix;
        force = true;
      };

      # contains universal preferences not being locked inside `programs.firefox.policies.Perferences`
      settings = {
        "breakpad.reportURL" = "";
        "devtools.debugger.remote-enabled" = false;
        "permissions.manager.defaultsUrl" = "";
        "security.cert_pinning.enforcement_level" = 2;
        "security.remote_settings.crlite_filters.enabled" = true;
        "security.pki.crlite_mode" = 2;
        "security.ssl.treat_unsafe_negotiation_as_broken" = true;

        "app.normandy.enabled" = false;
        "app.normandy.api_url" = "";

        "privacy.resistFingerprinting" = true;
        "privacy.resistFingerprinting.pbmode" = true;
        "privacy.resistFingerprinting.block_mozAddonManager" = true;
        "privacy.resistFingerprinting.letterboxing" = true;
        "privacy.window.maxInnerWidth" = 1600;
        "privacy.window.maxInnerHeight" = 900;

        "privacy.sanitize.timeSpan" = 0;

        # Privacy & Security > Cookies and Site Data > Clear Data...
        "privacy.clearSiteData.cache" = true;
        "privacy.clearSiteData.cookiesAndStorage" = true;
        "privacy.clearSiteData.historyFormDataAndDownloads" = true;
        "privacy.clearSiteData.siteSettings" = true;

        # Privacy & Security > History > Clear History...
        "privacy.clearHistory.cache" = true;
        "privacy.clearHistory.cookiesAndStorage" = true;
        "privacy.clearHistory.historyFormDataAndDownloads" = true;
        "privacy.clearHistory.siteSettings" = true;
        "privacy.cpd.cache" = true;
        "privacy.cpd.cookies" = true;
        "privacy.cpd.formdata" = true;
        "privacy.cpd.history" = true;
        "privacy.cpd.offlineApps" = true;
        "privacy.cpd.openWindows" = true;
        "privacy.cpd.passwords" = true;
        "privacy.cpd.sessions" = true;
        "privacy.cpd.siteSettings" = true;

        # Force the selection of a container for each new tab
        "privacy.userContext.newTabContainerOnLeftClick.enabled" = true;

        # Request English on every webpage
        "privacy.spoof_english" = 1;

        # Theses settings cause breakage so don't enforce it on all profiles
        # - disabling WebGL will break 3D stuff
        # - XOriginPolicy = 2 breaks websites like icloud, banks, ...
        "network.http.referer.XOriginPolicy" = 2;
        "webgl.disabled" = true;

        # No session restore after a restart
        "toolkit.winRegisterApplicationRestart" = false;

        # Lower the value's priority so each profile can have their own homepage
        "browser.startup.homepage" = "about:blank";
      };
    };
  };
}
