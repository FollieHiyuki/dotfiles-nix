# NOTE: this file is meant for external usage of `programs.firefox.profiles.<name>.search.engines`
# It is not used inside this Nix module.
{
  "AlpineLinux Wiki" = {
    urls = [ { template = "https://wiki.alpinelinux.org/w/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@alpine" ];
  };
  "ArchLinux Wiki" = {
    urls = [ { template = "https://wiki.archlinux.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@arch" ];
  };
  "Gentoo Wiki" = {
    urls = [ { template = "https://wiki.gentoo.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@gentoo" ];
  };
  "NixOS Wiki" = {
    urls = [ { template = "https://wiki.nixos.org/index.php?search={searchTerms}"; } ];
    definedAliases = [ "@nixos" ];
  };
  "Nix Packages" = {
    urls = [
      {
        template = "https://search.nixos.org/packages";
        params = [
          {
            name = "type";
            value = "packages";
          }
          {
            name = "query";
            value = "{searchTerms}";
          }
        ];
      }
    ];
    definedAliases = [ "@nixpkgs" ];
  };
  "GitHub" = {
    urls = [ { template = "https://github.com/search?q={searchTerms}"; } ];
    definedAliases = [ "@github" ];
  };
  "Godocs" = {
    urls = [ { template = "https://godocs.io/?q={searchTerms}"; } ];
    definedAliases = [ "@godoc" ];
  };
  "MDN" = {
    urls = [ { template = "https://developer.mozilla.org/en-US/search?q={searchTerms}"; } ];
    definedAliases = [ "@mdn" ];
  };
  "Stackoverflow" = {
    urls = [ { template = "http://stackoverflow.com/search?q={searchTerms}"; } ];
    definedAliases = [ "@so" ];
  };
  "Youtube" = {
    urls = [ { template = "https://www.youtube.com/results?search_query={searchTerms}"; } ];
    definedAliases = [ "@yt" ];
  };
  "SearxNG" = {
    urls = [
      { template = "https://search.disroot.org/search?category_general=on&q={searchTerms}"; }
      { template = "https://searx.envs.net/search?category_general=on&q={searchTerms}"; }
      { template = "https://search.ononoki.org//search?category_general=on&q={searchTerms}"; }
      { template = "https://searx.be//search?category_general=on&q={searchTerms}"; }
    ];
    definedAliases = [ "@searx" ];
  };
  "Cambridge Dict" = {
    urls = [ { template = "https://dictionary.cambridge.org/dictionary/english/{searchTerms}"; } ];
    definedAliases = [ "@dict" ];
  };

  # "DuckDuckGo".metaData.alias = "@ddg";
  "Google".metaData.alias = "@gg";
  "Wikipedia (en)".metaData.alias = "@wiki";
}
