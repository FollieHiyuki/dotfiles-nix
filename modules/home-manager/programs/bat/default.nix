{ config, lib, ... }:
lib.mkMerge [
  {
    programs.bat.config = {
      color = "always";
      italic-text = "always";
      style = "plain";
      theme = "Nord";
    };
  }

  # Use bat for less syntax highlighting
  (lib.mkIf config.programs.bat.enable {
    home.sessionVariables.LESSOPEN = "|${lib.getExe config.programs.bat.package} %s";
  })
]
