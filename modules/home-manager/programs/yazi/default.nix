{ pkgs, lib, ... }:
let
  yaziPlugins = pkgs.fetchFromGitHub {
    owner = "yazi-rs";
    repo = "plugins";
    rev = "a1b678dfacfd2726fad364607aeaa7e1fded3cfa";
    hash = "sha256-Vvq7uau+UNcriuLE7YMK5rSOXvVaD0ElT59q+09WwdQ=";
  };
in
{
  programs.yazi = {
    initLua = ./init.lua;

    keymap.manager.prepend_keymap = [
      {
        on = "!";
        run = "shell \"$SHELL\" --block";
        desc = "Open shell in current directory";
      }
      {
        on = "F";
        run = "plugin smart-filter";
        desc = "Smart filter";
      }
      {
        on = "l";
        run = "plugin smart-enter";
        desc = "Enter the child directory, or open the file";
      }
      {
        on = "<Enter>";
        run = "plugin smart-enter";
        desc = "Enter the child directory, or open the file";
      }
      {
        on = [
          "c"
          "m"
        ];
        run = "plugin chmod";
        desc = "Chmod on selected files";
      }
      {
        on = "M";
        run = "plugin mount";
        desc = "Manage and mount block devices";
      }
      {
        on = [
          "g"
          "r"
        ];
        run = "shell -- ya emit cd \"$(git rev-parse --show-toplevel)\"";
        desc = "Go to current project root";
      }
    ];

    plugins = lib.genAttrs [
      "full-border"
      "smart-filter"
      "smart-enter"
      "mount"
      "chmod"
    ] (item: "${yaziPlugins}/${item}.yazi");

    settings.manager = {
      ratio = [
        0
        5
        5
      ];
      sort_by = "natural";
      linemode = "size";
      show_hidden = true;
      show_symlink = false;
    };

    theme = {
      manager = {
        cwd.fg = "#88c0d0";

        # Hover
        preview_hovered = { };

        # Find
        find_keyword = {
          fg = "#ebcb8b";
          bold = true;
          underline = true;
        };
        find_position = {
          fg = "#b48ead";
          bg = "reset";
          bold = true;
        };

        # Marker
        marker_copied = {
          fg = "#aebe8c";
          bg = "#a3be8c";
        };
        marker_cut = {
          fg = "#bf616a";
          bg = "#bf616a";
        };
        marker_marked = {
          fg = "#88c0d0";
          bg = "#88c0d0";
        };
        marker_selected = {
          fg = "#ebcb8b";
          bg = "#ebcb8b";
        };

        # Tab
        tab_active = {
          fg = "#88c0d0";
          reversed = true;
        };
        tab_inactive.fg = "#88c0d0";

        # Count
        count_copied = {
          fg = "#d8dee9";
          bg = "#a3be8c";
        };
        count_cut = {
          fg = "#d8dee9";
          bg = "#bf616a";
        };
        count_selected = {
          fg = "#d8dee9";
          bg = "#ebcb8b";
        };

        # Border
        border_style.fg = "#d8dee9";

        # Highlighting
        syntect_theme = ./nord.tmTheme;
      };

      mode = {
        normal_main = {
          fg = "#2e3440";
          bg = "#81a1c1";
          bold = true;
        };
        normal_alt = {
          fg = "#81a1c1";
          bg = "#3b4252";
        };
        select_main = {
          fg = "#2e3440";
          bg = "#ebcb8b";
          bold = true;
        };
        select_alt = {
          fg = "#ebcb8b";
          bg = "#3b4252";
        };
        unset_main = {
          fg = "#2e3440";
          bg = "#bf616a";
          bold = true;
        };
        unset_alt = {
          fg = "#bf616a";
          bg = "#3b4252";
        };
      };

      status = {
        overall = { };

        # Progress
        progress_normal = {
          fg = "#81a1c1";
          bg = "#3b4252";
        };
        progress_error = {
          fg = "#bf616a";
          bg = "#3b4252";
        };

        # Permissions
        perm_type.fg = "#a3be8c";
        perm_read.fg = "#ebcb8b";
        perm_write.fg = "#bf616a";
        perm_exec.fg = "#88c0d0";
        perm_sep.fg = "#7b88a1";
      };

      pick = {
        border.fg = "#81a1c1";
        active = {
          fg = "#b48ead";
          bold = true;
        };
      };

      input = {
        border.fg = "#81a1c1";
        title.bold = true;
      };

      cmp.border.fg = "#81a1c1";

      tasks = {
        border.fg = "#81a1c1";
        hover = {
          fg = "#b48ead";
          underline = true;
        };
      };

      which = {
        mask.bg = "#3b4252";
        cand.fg = "#88c0d0";
        rest.fg = "#7b88a1";
        desc.fg = "#b48ead";
        separator_style.fg = "#7b88a1";
      };

      help = {
        on.fg = "#88c0d0";
        run.fg = "#b48ead";
        footer = {
          fg = "#2e3440";
          bg = "#d8dee9";
        };
      };

      notify = {
        title_info.fg = "#a3be8c";
        title_warn.fg = "#ebcb8b";
        title_error.fg = "#bf616a";
      };

      filetype = {
        rules = [
          {
            # Ref: https://en.wikipedia.org/wiki/List_of_archive_formats
            mime = "application/{${
              builtins.concatStringsSep "," [
                # tar
                "x-tar*"
                "x-gtar*"
                "*-compressed-tar"

                # zip
                "zip"
                "zlib"
                "java-archive"
                "vnd.android.package-archive"
                "x-zip*"

                "zstd"

                # xz
                "x-xz*"
                "*-xz-compressed"

                # rar
                "vnd.rar"
                "x-rar*"

                "x-7z-compressed"

                # iso
                "vnd.efi.iso"
                "x-*-rom"
                "x-compressed-iso"
                "x-iso9660-image"
                "x-iso9660-appimage"

                "x-cpio*"

                "gzip"

                # fallback
                "vnd.ms-cab-compressed"
                "x-archive"
                "x-bzip*"
                "x-lzma"
                "x-lzip"
                "x-lha"
                "x-lzh*"
                "x-lzop"
                "x-lz4"
                "x-lrzip"
              ]
            }}";
            fg = "#d08770";
          }
          {
            mime = "application/{${
              builtins.concatStringsSep "," [
                "pdf"
                "x-*pdf"
                "x-*dvi"
                "x-cb*"
                "x-fictionbook+xml"
                "epub+zip"
                "postscript"
                "x-*postscript"
                "vnd.oasis.opendocument.*"
                "vnd.openxmlformats-officedocument.*"
              ]
            }}";
            fg = "#5e81ac";
          }
          {
            mime = "image/vnd.djvu";
            fg = "#5e81ac";
          }
          {
            mime = "image/*";
            fg = "#8fbcbb";
          }
          {
            mime = "{audio/video}/*";
            fg = "#b48ead";
          }

          {
            name = "*";
            is = "sock";
            fg = "#b48ead";
            bg = "#3b4252";
          }
          {
            name = "*";
            is = "fifo";
            fg = "#ebcb8b";
            bg = "#3b4252";
          }
          {
            name = "*";
            is = "link";
            fg = "#8fbcbb";
          }
          {
            name = "*/";
            is = "link";
            fg = "#8fbcbb";
            bold = true;
          }
          {
            name = "*";
            is = "char";
            fg = "#ebcb8b";
          }
          {
            name = "*";
            is = "block";
            fg = "#ebcb8b";
          }
          {
            name = "*";
            is = "exec";
            fg = "#a3be8c";
          }
          {
            name = "*";
            is = "orphan";
            fg = "#bf616a";
          }

          {
            name = "*";
            fg = "#d8dee9";
          }
          {
            name = "*/";
            fg = "#81a1c1";
            bold = true;
          }
        ];
      };

      icon = {
        dirs = [
          {
            name = ".git";
            text = "";
          }
          {
            name = ".github";
            text = "";
          }
          {
            name = ".gitlab";
            text = "";
          }
          {
            name = ".vscode";
            text = "";
          }
          {
            name = ".cache";
            text = "󰝉";
          }
          {
            name = ".config";
            text = "";
          }
          {
            name = ".local";
            text = "󰉌";
          }
          {
            name = ".kube";
            text = "󱃾";
          }
          {
            name = ".mozilla";
            text = "󰈹";
          }
          {
            name = ".librewolf";
            text = "󰈹";
          }
          {
            name = ".thunderbird";
            text = "";
          }
          {
            name = ".tor project";
            text = "";
          }
          {
            name = "nix";
            text = "";
          }
          {
            name = ".nix-profile";
            text = "";
          }
          {
            name = "gnu";
            text = "";
          }
          {
            name = "guix";
            text = "";
          }
          {
            name = ".guix-profile";
            text = "";
          }
          {
            name = "node_modules";
            text = "";
          }
          {
            name = "Android";
            text = "";
          }
          {
            name = "Books";
            text = "";
          }
          {
            name = "Code";
            text = "";
          }
          {
            name = "Desktop";
            text = "";
          }
          {
            name = "Documents";
            text = "";
          }
          {
            name = "Downloads";
            text = "";
          }
          {
            name = "Music";
            text = "󰌳";
          }
          {
            name = "Pictures";
            text = "";
          }
          {
            name = "Public";
            text = "";
          }
          {
            name = "Templates";
            text = "";
          }
          {
            name = "Videos";
            text = "󰨜";
          }
        ];

        conds = [
          # Special files
          {
            "if" = "orphan";
            text = "";
          }
          {
            "if" = "block";
            text = "";
          }
          {
            "if" = "char";
            text = "";
          }
          {
            "if" = "fifo";
            text = "󰈲";
          }
          {
            "if" = "sock";
            text = "󰆨";
          }
          {
            "if" = "dummy";
            text = "?";
          }

          # Fallbacks
          {
            "if" = "dir";
            text = "󰝰";
          }
          {
            "if" = "link";
            text = "";
          }
          {
            "if" = "exec";
            text = "";
          }
          {
            "if" = "!dir";
            text = "󰈔";
          }
        ];
      };
    };
  };
}
