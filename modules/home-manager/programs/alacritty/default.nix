{ lib, ... }:
{
  programs.alacritty.settings = {
    general.live_config_reload = false;
    window = {
      padding = {
        x = 2;
        y = 2;
      };
      opacity = 0.96;
      blur = true; # only works on Darwin or KDE Wayland
      dynamic_title = false;
      option_as_alt = "Both";
    };
    scrolling = {
      history = 50000;
      multiplier = 5;
    };
    font = {
      normal = {
        family = lib.mkDefault "IosevkaTerm Nerd Font";
        style = "Regular";
      };
      bold = {
        family = lib.mkDefault "IosevkaTerm Nerd Font";
        style = "Bold";
      };
      italic = {
        family = lib.mkDefault "IosevkaTerm Nerd Font";
        style = "Italic";
      };
      bold_italic = {
        family = lib.mkDefault "IosevkaTerm Nerd Font";
        style = "Bold Italic";
      };
    };
    colors = {
      primary = {
        foreground = "#d8dee9";
        background = "#2e3440";
      };
      cursor = {
        text = "#2e3440";
        cursor = "#d8dee9";
      };
      vi_mode_cursor = {
        text = "#2e3440";
        cursor = "#d8dee9";
      };
      search = {
        matches = {
          foreground = "#2e3440";
          background = "#81a1c1";
        };
        focused_match = {
          foreground = "#2e3440";
          background = "#ebcb8b";
        };
      };
      hints = {
        start = {
          foreground = "#2e3440";
          background = "#a3be8c";
        };
        end = {
          foreground = "#a3be8c";
          background = "#2e3440";
        };
      };
      footer_bar = {
        foreground = "#2e3440";
        background = "#d8dee9";
      };
      normal = {
        black = "#3b4252";
        red = "#bf616a";
        green = "#a3be8c";
        yellow = "#ebcb8b";
        blue = "#81a1c1";
        magenta = "#b48ead";
        cyan = "#88c0d0";
        white = "#e5e9f0";
      };
      bright = {
        black = "#4c566a";
        red = "#bf616a";
        green = "#a3be8c";
        yellow = "#ebcb8b";
        blue = "#81a1c1";
        magenta = "#b48ead";
        cyan = "#8fbcbb";
        white = "#eceff4";
      };
      dim = {
        black = "#373e4d";
        red = "#94545d";
        green = "#809575";
        yellow = "#b29e75";
        blue = "#68809a";
        magenta = "#8c738c";
        cyan = "#6d96a5";
        white = "#aeb3bb";
      };
    };
    bell = {
      animation = "EaseOutExpo";
      color = "#d8dee9";
    };
    cursor = {
      style.shape = "Beam";
      vi_mode_style.shape = "Block";
    };
    mouse.hide_when_typing = true;
  };
}
