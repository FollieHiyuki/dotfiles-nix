{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  # Always use ungoogled-chromium
  chromiumPackage = pkgs.ungoogled-chromium;

  supportedBrowsers = [
    "brave"
    "chromium"
  ];

  browserModule = name: {
    enable = mkEnableOption name;

    # Chromium policies on MacOS are set using "defaults" command at Recommended level.
    # (Mandatory-level policies are only available for managed machine with JamF).
    policies = mkOption {
      type = types.attrs;
      description = ''
        ${name} policy options.
        See <https://cloud.google.com/docs/chrome-enterprise/policies/> for general options
        and <https://support.brave.com/hc/en-us/articles/360039248271-Group-Policy> for
        Brave-specific options.
      '';
      default = { };
      visible = pkgs.stdenv.isDarwin;
    };

    extensions = mkOption {
      type =
        with types;
        let
          extensionType = submodule {
            options = {
              id = mkOption {
                type = strMatching "[a-zA-Z]{32}";
                description = ''
                  The extension's ID from the Chrome Web Store url or the unpacked crx.
                '';
                default = "";
              };

              updateUrl = mkOption {
                type = str;
                description = ''
                  URL of the extension's update manifest XML file. Linux only.
                '';
                default = "https://clients2.google.com/service/update2/crx";
              };

              crxPath = mkOption {
                type = nullOr path;
                description = ''
                  Path to the extension's crx file. Linux only.
                '';
                default = null;
              };

              version = mkOption {
                type = nullOr str;
                description = ''
                  The extension's version, required for local installation. Linux only.
                '';
                default = null;
              };
            };
          };
        in
        listOf (coercedTo str (v: { id = v; }) extensionType);
      default = [ ];
      example = literalExpression ''
        [
          { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock origin
          {
            id = "dcpihecpambacapedldabdbpakmachpb";
            updateUrl = "https://raw.githubusercontent.com/iamadamdev/bypass-paywalls-chrome/master/updates.xml";
          }
          {
            id = "aaaaaaaaaabbbbbbbbbbcccccccccc";
            crxPath = "/home/share/extension.crx";
            version = "1.0";
          }
        ]
      '';
      description = ''
        List of chromium extensions to install.
        To find the extension ID, check its URL on the
        [Chrome Web Store](https://chrome.google.com/webstore/category/extensions).

        To install extensions outside of the Chrome Web Store set
        `updateUrl` as explained in the
        [Chrome documentation](https://developer.chrome.com/docs/extensions/mv2/external_extensions).
      '';
    };

    dictionaries = mkOption {
      type = types.listOf types.package;
      default = [ ];
      example = literalExpression ''
        [
          pkgs.hunspellDictsChromium.en_US
        ]
      '';
      description = ''
        List of chromium dictionaries to install.
      '';
    };
  };

  browserConfig =
    name: cfg:
    let
      browserIds = {
        chromium = "org.chromium.Chromium";
        brave = "com.brave.Browser";
      };

      darwinDirs = {
        chromium = "Chromium";
        brave = "BraveSoftware/Brave-Browser";
      };

      linuxDirs = {
        brave = "BraveSoftware/Brave-Browser";
      };

      configDir =
        if pkgs.stdenv.isDarwin then
          "Library/Application Support/" + darwinDirs.${name}
        else
          "${config.xdg.configHome}/" + (linuxDirs.${name} or name);
    in
    mkIf cfg.enable (mkMerge [
      (mkIf (pkgs.stdenv.isDarwin && cfg.policies != { }) {
        home.file."Library/Preferences/${browserIds.${name}}.plist".text =
          generators.toPlist { }
            cfg.policies;
      })

      {
        home.file = listToAttrs (
          (builtins.map (
            ext:
            assert ext.crxPath != null -> ext.version != null;
            {
              name = "${configDir}/External Extensions/${ext.id}.json";
              value.text = builtins.toJSON (
                if ext.crxPath != null then
                  {
                    external_crx = ext.crxPath;
                    external_version = ext.version;
                  }
                else
                  { external_update_url = ext.updateUrl; }
              );
            }
          ) cfg.extensions)
          ++ (builtins.map (pkg: {
            name = "${configDir}/Dictionaries/${pkg.passthru.dictFileName}";
            value.source = pkg;
          }) cfg.dictionaries)
        );
      }
    ]);
in
{
  # Override the default programs.chromium module for simpler configuration and ungoogled-chromium support.
  # Ref: https://github.com/nix-community/home-manager/pull/4174
  disabledModules = [ "programs/chromium.nix" ];

  options.programs = genAttrs supportedBrowsers browserModule;

  config = mkMerge (
    (builtins.map (name: browserConfig name config.programs.${name}) supportedBrowsers)
    ++ [
      # chromium package isn't available on MacOS, so refer to the HomeBrew's eloston-chromium package here.
      (mkIf config.programs.chromium.enable {
        home.sessionVariables.PUPPETEER_EXECUTABLE_PATH =
          if pkgs.stdenv.isDarwin then
            "/Applications/Chromium.app/Contents/MacOS/Chromium"
          else
            "${getExe chromiumPackage}";

        home.packages = mkIf pkgs.stdenv.isLinux [ chromiumPackage ];
      })

      (mkIf config.programs.brave.enable {
        home.packages = [ pkgs.brave ];
      })

      (mkIf pkgs.stdenv.isDarwin {
        programs.brave.policies = {
          BraveRewardsDisabled = 1;
          BraveWalletDisabled = 1;
          BraveVPNDisabled = 1;
          BraveAIChatEnabled = 0;
        };
      })

      {
        programs.chromium = {
          dictionaries = [ pkgs.hunspellDictsChromium.en_US ];

          # ungoogled-chromium ignores updateUrl option and ExtensionInstallForceList
          # policy (which requires access to Chrome Web Store).
          # Ref: https://github.com/NixOS/nixpkgs/pull/188086
          #
          # To get the extension's URL, run: curl -Lo /dev/null -w '%{url_effective}\n' 'https://clients2.google.com/service/update2/crx?response=redirect&prodversion=132.0.6834.159&acceptformat=crx3&x=id%3D''<EXTENSION_ID_HERE>''%26installsource%3Dondemand%26uc'
          extensions = [
            # ClearURLs
            {
              id = "lckanjgmijmafbedllaakclkaicjfmnk";
              version = "1.26.0";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5oif8O64NxRkyrFT37He0IlYgG8t91ez5iiqH3-Q_IiPlqDbanUq6VmHsJYQt9u_TrRn0gt0T6Nn4nVsRL6kUKZuabp8Xm0VNV_buLAphwVR7QIQAMZSmuW_gBn8b88j-0b8RzYGoQIJ80Qvbw/LCKANJGMIJMAFBEDLLAAKCLKAICJFMNK_1_26_0_0.crx";
                sha256 = "06m3b3npis7cpv0yif0rs3dkfdgd69r0rkyxlwwry26h58dp7hdc";
              };
            }

            # Cookie AutoDelete
            {
              id = "fhcgjolkccmbidfldomjliifgaodjagh";
              version = "3.8.2";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5ohQmniCM9_y4sKxy6QX8XrCLSDHtBymPFOb9FE9YxobC1aQEF01ZMOyI_5L1rSEF_pgQpke9bFJ6f44WN0v1ladY3Scq6npfe_gW086ii5KqfzyAMZSmuXNH-LQEnXFZJTObWl7BmXROtDC5A/FHCGJOLKCCMBIDFLDOMJLIIFGAODJAGH_3_8_2_0.crx";
                sha256 = "0bgd7k68wl3invdism0awjdxbdfbm1c7nimfzqhzmv2mvwr6059y";
              };
            }

            # Decentraleyes
            {
              id = "ldpochfccmkkmhdbclfhpagapcfdljkj";
              version = "3.0.0";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5oiFUkzi9T1sWGikHsJtnAERynC9BEOLhvOSBP2-Ds-vzK3HuLYTPUmc85DzmVn5imII9LCD20G8J-M-OIZbAeIoETw3CtKNkYtTP_OOIYE_ZIjOrdn81WBzc-9HGZKsAMZSmuWnfKrMgx0k3xBcyA6hgJyeCazNUg/LDPOCHFCCMKKMHDBCLFHPAGAPCFDLJKJ_3_0_0_0.crx";
                sha256 = "198k1hyzf3a1yz4chnx095rwqa15hkcck4ir6xs6ps29qgqw8ili";
              };
            }

            # SponsorBlock for Youtube
            {
              id = "mnjggcdmjocbbbhaepdhchncahnbgone";
              version = "5.11.5";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5oj7hUJFB7Q3NhkDYydZ-hkDzxVYDe3e7lHrRjj94fOzD6KXI8ejAHYTq5UGkW8R9jJIqs7N5SgcA7f5eeNbD0ZyolY7TImCjXu6ZruMca18XRDvSshLT1GubuPabF8AxlKa5QcSZT4NVqUG1-TlIhfGtZ93jKcd/MNJGGCDMJOCBBBHAEPDHCHNCAHNBGONE_5_11_5_0.crx";
                sha256 = "07g8278s55wzzjb7im3lirmq7aq0pb3zfgw8xly0wczwjz3mhzb6";
              };
            }

            # Skip Redirect
            {
              id = "jaoafjdoijdconemdmodhbfpianehlon";
              version = "2.3.6";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5og3X07o1KbVO_3StD8kPuz2hRQN_r2lDdvCInC-FkWrq4SS0_-6BdGTzXmIEDxshsStzNjJr86NY-UlcmThKCalHI95F3O2gksveOxlsE6JIRqXAMZSmuUWn121B0TSBa-PPz761A6KWrgOhA/JAOAFJDOIJDCONEMDMODHBFPIANEHLON_2_3_6_0.crx";
                sha256 = "1yy0q2kxsy99bpxrm9acx4c8rgf2y5pd843bi4bdvaagfijh069x";
              };
            }

            # Ublock Origin Lite
            {
              id = "ddkjiahejlhfcafbddmgiahcphecmpfh";
              version = "2025.2.19.775";
              crxPath = builtins.fetchurl {
                url = "https://clients2.googleusercontent.com/crx/blobs/ASuc5oiO5WOvTNjgZXdxSMj5aoXV9Mq6vpbZXCiHFa9n3zQIyZXrADGjrwcfeWUvM3X04t5MpmIyxdjpQjTlvcLOkf5hYWvPs4f6dMHQ-g2wbJeFswp5u3wIHgl28erenY0KAMZSmuWLydNRPS-Mf6dNlskjRQht7ft2uQ/DDKJIAHEJLHFCAFBDDMGIAHCPHECMPFH_2025_2_19_775.crx";
                sha256 = "0knvjj3b13gh6vipqis3m4b2xx74ays1n7q6khkmq43cc0p5bwfl";
              };
            }

            # LibRedirect
            {
              id = "oladmjdebphlnjjcnomfhhbfdldiimaf";
              version = "3.1.0";
              crxPath = builtins.fetchurl {
                url = "https://github.com/libredirect/libredirect/releases/download/v3.1.0/libredirect-3.1.0.crx";
                sha256 = "003q48gzyr282yk1l267myx4ba8dfb656lpxspx2gjhqmfdz9g8b";
              };
            }

            # Chromium Web Store
            {
              id = "ocaahdebbfolfmndjeplogmgcagdmblk";
              version = "1.5.4.3";
              crxPath = builtins.fetchurl {
                url = "https://github.com/NeverDecaf/chromium-web-store/releases/download/v1.5.4.3/Chromium.Web.Store.crx";
                sha256 = "0ck5k4gs5cbwq1wd5i1aka5hwzlnyc4c513sv13vk9s0dlhbz4z5";
              };
            }
          ];
        };

        programs.brave = {
          dictionaries = [ pkgs.hunspellDictsChromium.en_US ];

          extensions = [
            # ClearURLs
            { id = "lckanjgmijmafbedllaakclkaicjfmnk"; }

            # SponsorBlock for Youtube
            { id = "mnjggcdmjocbbbhaepdhchncahnbgone"; }

            # Skip Redirect
            { id = "jaoafjdoijdconemdmodhbfpianehlon"; }

            # LibRedirect
            {
              id = "oladmjdebphlnjjcnomfhhbfdldiimaf";
              updateUrl = "https://raw.githubusercontent.com/libredirect/browser_extension/master/src/updates/updates.xml";
            }
          ];
        };
      }
    ]
  );
}
