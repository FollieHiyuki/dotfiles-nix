{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  color = "#5e81ac"; # nord10

  # Only set non-default options
  # Ref: https://github.com/ferdium/ferdium-app/blob/develop/src/config.ts
  ferdiumSettings = {
    runInBackground = false;
    clipboardNotifications = false;
    server = "You are using Ferdium without a server";
    autohideMenuBar = true;
    automaticUpdates = false;
    universalDarkMode = false;
    userAgentPref = config.home.userAgentString;
    accentColor = color;
    progressbarAccentColor = color;
    serviceRibbonWidth = 55; # Slim sidebar
    sentry = false;
    navigationBarBehaviour = "always";
    searchEngine = "duckDuckGo";
    useHorizontalStyle = true;
    hideCollapseButton = true;
    hideSplitModeButton = false;
    enableLongPressServiceHint = true;
    keepAllWorkspacesLoaded = true;
  };
in
{
  options.programs.ferdium.enable = mkEnableOption "ferdium";

  config = mkIf (config.programs.ferdium.enable && pkgs.stdenv.isLinux) {
    home.packages = [ pkgs.ferdium ];

    # The symlink prevents any options to be overridden by Ferdium UI
    # => no locking feature and no global app mute toggle
    xdg.configFile."Ferdium/config/settings.json".text = builtins.toJSON ferdiumSettings;

    # Override the desktop entry to enable Wayland and IME support
    # Note that electron doesn't support running internal chromium with GTK4
    xdg.desktopEntries.ferdium = {
      name = "Ferdium";
      exec = builtins.concatStringsSep " " [
        "${pkgs.ferdium}/bin/ferdium"
        "--ozone-platform-hint=auto"
        "--enable-features=WebRTCPipeWireCapturer,WaylandWindowDecorations,UseOzonePlatform"
        "--enable-wayland-ime"
        "%U"
      ];
      terminal = false;
      type = "Application";
      icon = "ferdium";
      comment = "Desktop app bringing all your messaging services into one installable";
      mimeType = [
        "x-scheme-handler/ferdium"
      ];
      categories = [
        "Network"
        "InstantMessaging"
      ];

      settings.StartupWMClass = "Ferdium";
    };
  };
}
