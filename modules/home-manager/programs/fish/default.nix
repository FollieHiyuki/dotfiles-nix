{
  config,
  pkgs,
  lib,
  ...
}:
{
  programs.fish = {
    shellInit = ''
      # Use both Emacs and Vi bindings
      set -g fish_key_bindings fish_hybrid_key_bindings

      # Set cursor shape
      set -g fish_cursor_default block
      set -g fish_cursor_insert line
      set -g fish_cursor_replace_one underscore
      set -g fish_cursor_visual block
      set -g fish_vi_force_cursor

      # Disable the default greeting message
      set -g fish_greeting

      # Nord theme
      set -g fish_color_autosuggestion 4c566a
      set -g fish_color_cancel --reverse
      set -g fish_color_command 88c0d0
      set -g fish_color_comment 4c566a --italics
      set -g fish_color_cwd 5e81ac
      set -g fish_color_cwd_root bf616a
      set -g fish_color_end 81a1c1
      set -g fish_color_error bf616a
      set -g fish_color_escape ebcb8b
      set -g fish_color_history_current eceff4 --bold
      set -g fish_color_host a3be8c
      set -g fish_color_host_remote ebcb8b
      set -g fish_color_keyword 81a1c1
      set -g fish_color_match --background=brblue
      set -g fish_color_normal normal
      set -g fish_color_operator 81a1c1
      set -g fish_color_option 8fbcbb
      set -g fish_color_param eceff4
      set -g fish_color_quote a3be8c
      set -g fish_color_redirection b48ead --bold
      set -g fish_color_search_match --bold --background=434c5e
      set -g fish_color_selection eceff4 --bold --background=434c5e
      set -g fish_color_status bf616a
      set -g fish_color_user a3be8c
      set -g fish_color_valid_path --underline
      set -g fish_pager_color_background
      set -g fish_pager_color_completion eceff4
      set -g fish_pager_color_description ebcb8b --italics
      set -g fish_pager_color_prefix normal --bold --underline
      set -g fish_pager_color_progress 3b4252 --background=d08770
      set -g fish_pager_color_secondary_background
      set -g fish_pager_color_secondary_completion
      set -g fish_pager_color_secondary_description
      set -g fish_pager_color_secondary_prefix
      set -g fish_pager_color_selected_background --background=434c5e
      set -g fish_pager_color_selected_completion
      set -g fish_pager_color_selected_description
      set -g fish_pager_color_selected_prefix
    '';

    functions = {
      # Special functions, recognized by fish
      fish_greeting.body = "status is-interactive; and ${lib.escapeShellArg config.xdg.binHome}/bunny.sh";
      fish_user_key_bindings.body = ''
        # Make <Alt-j> and <Alt-k> behaves like <Alt+↓> and <Alt+↑> (shorter hand distant)
        bind -M insert \ej history-token-search-forward
        bind -M insert \ek history-token-search-backward
      '';

      # Dummy functions, used solely to register abbrs
      __multicd = ''echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)'';
      __last_history_item = ''echo $history[1]'';
    };

    shellAbbrs = {
      # string values are used as shellAliases for other shells
      ad = "ansible-doc";
      ap = "ansible-playbook";
      e = "emacs -nw";
      k = "kubectl";
      kn = "kubens";
      kx = "kubectx";
      p = "pulumi";
      t = "tofu";

      # special abbr, available for fish-shell only
      # Ref: https://github.com/fish-shell/fish-shell/issues/9462
      "!!" = {
        position = "anywhere";
        function = "__last_history_item";
      };
      dotdot = {
        regex = ''^\.\.+$'';
        function = "__multicd";
      };
    };

    plugins = [
      {
        name = "autopair";
        src = pkgs.fetchFromGitHub {
          owner = "jorgebucaran";
          repo = "autopair.fish";
          rev = "4d1752ff5b39819ab58d7337c69220342e9de0e2";
          hash = "sha256-qt3t1iKRRNuiLWiVoiAYOu+9E7jsyECyIqZJ/oRIT1A=";
        };
      }
    ];
  };
}
