_: {
  programs.lsd = {
    enableAliases = true;
    settings = {
      blocks = [
        "permission"
        "links"
        "user"
        "group"
        "size"
        "date"
        "git"
        "name"
      ];
      date = "+%b %d %Y %H:%M";
      size = "short";
      sorting.dir-grouping = "first";
      symlink-arrow = "->";
    };
    colors = import ./colors.nix;
  };
}
