{
  user = [
    129
    161
    193
  ];
  group = [
    129
    161
    193
  ];
  permission = {
    read = [
      163
      190
      140
    ];
    write = [
      235
      203
      139
    ];
    exec = [
      191
      97
      106
    ];
    exec-sticky = [
      180
      142
      173
    ];
    no-access = [
      76
      86
      106
    ];
    octal = [
      136
      192
      208
    ];
    acl = [
      136
      192
      208
    ];
    context = [
      129
      161
      193
    ];
  };
  date = {
    hour-old = [
      163
      190
      140
    ];
    day-old = [
      163
      190
      140
    ];
    older = [
      163
      190
      140
    ];
  };
  size = {
    none = [
      76
      86
      106
    ];
    small = [
      235
      203
      139
    ];
    medium = [
      208
      135
      112
    ];
    large = [
      191
      97
      106
    ];
  };
  inode = {
    valid = [
      180
      142
      173
    ];
    invalid = [
      76
      86
      106
    ];
  };
  links = {
    valid = [
      143
      188
      187
    ];
    invalid = [
      76
      86
      106
    ];
  };
  tree-edge = [
    76
    86
    106
  ];
  git-status = {
    default = [
      76
      86
      106
    ];
    unmodified = [
      76
      86
      106
    ];
    ignored = [
      76
      86
      106
    ];
    new-in-index = [
      163
      190
      140
    ];
    new-in-workdir = [
      163
      190
      140
    ];
    typechange = [
      235
      203
      139
    ];
    deleted = [
      191
      97
      106
    ];
    renamed = [
      163
      190
      140
    ];
    modified = [
      235
      203
      139
    ];
    conflicted = [
      191
      97
      106
    ];
  };
}
