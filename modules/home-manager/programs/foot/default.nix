{ config, ... }:
let
  # Reuse the confiugraion of alacritty
  cfg = config.programs.alacritty.settings;

  font_size = builtins.toString cfg.font.size;
in
{
  programs.foot = {
    server.enable = true;
    settings = {
      main = {
        font = "${cfg.font.normal.family}:style=Regular:size=${font_size}";
        font-bold = "${cfg.font.bold.family}:style=Bold:size=${font_size}";
        font-italic = "${cfg.font.italic.family}:style=Italic:size=${font_size}";
        font-bold-italic = "${cfg.font.bold_italic.family}:style=Bold Italic:size=${font_size}";
        underline-offset = "2px";
        locked-title = !cfg.window.dynamic_title;
        pad = "${builtins.toString cfg.window.padding.x}x${builtins.toString cfg.window.padding.y}";
      };
      bell = {
        urgent = true;
        notify = true;
      };
      scrollback.lines = cfg.scrolling.history;
      cursor = {
        style = "block";
        beam-thickness = 1.0;
        color = "2e3440 d8dee9";
      };
      mouse.hide-when-typing = cfg.mouse.hide_when_typing;
      colors = {
        alpha = 0.98;

        foreground = "d8dee9";
        background = "2e3440";

        regular0 = "3b4252";
        regular1 = "bf616a";
        regular2 = "a3be8c";
        regular3 = "ebcb8b";
        regular4 = "81a1c1";
        regular5 = "b48ead";
        regular6 = "88c0d0";
        regular7 = "e5e9f0";

        bright0 = "4c566a";
        bright1 = "bf616a";
        bright2 = "a3be8c";
        bright3 = "ebcb8b";
        bright4 = "81a1c1";
        bright5 = "b48ead";
        bright6 = "8fbcbb";
        bright7 = "eceff4";

        dim0 = "373e4d";
        dim1 = "94545d";
        dim2 = "809575";
        dim3 = "b29e75";
        dim4 = "68809a";
        dim5 = "8c738c";
        dim6 = "6d96a5";
        dim7 = "aeb3bb";
      };

      # FIXME: foot doesn't expose anything beside TERM for other programs to know
      # we're inside foot session. As such, set an unique variable here.
      # Remember to update preview.sh script if this varaible is deleted.
      environment."TERMINAL_NAME" = "foot";
    };
  };
}
