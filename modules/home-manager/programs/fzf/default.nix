{ config, lib, ... }:
{
  programs.fzf = {
    defaultCommand = "fd --type f --follow --hidden --exclude .git --color never";
    defaultOptions = [
      "--multi"
      "--layout=reverse"
      "--info=inline"
      "--cycle"
      "--preview-label=' Preview '"

      # Nord theme
      "--color=fg:-1,bg:-1,hl:#a3be8c,fg+:#eceff4,bg+:#434c5e,hl+:#a3be8c"
      "--color=pointer:#bf616a,info:#7b88a1,spinner:#4c566a,header:#4c566a,prompt:#81a1c1,marker:#ebcb8b"
      "--color=separator:#7b88a1,label:#eceff4,border:#7b88a1"
    ];

    changeDirWidgetCommand = "fd --type d --follow --hidden --exclude .git --color never";
    changeDirWidgetOptions = [
      "--preview 'lsd -1FAL --group-dirs first --icon always --color always {}'"
    ];

    fileWidgetCommand = "fd . \$dir --follow --hidden --exclude .git --color never";
    fileWidgetOptions = [
      "--no-height --preview '${lib.escapeShellArg config.xdg.binHome}/preview.sh {} \\$FZF_PREVIEW_COLUMNS \\$FZF_PREVIEW_LINES 2>/dev/null'"
    ];
  };
}
