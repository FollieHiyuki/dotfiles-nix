_: {
  # TODO: also apply Firefox options here
  programs.librewolf.settings = {
    "privacy.resistFingerprinting" = true;
    "privacy.resistFingerprinting.letterboxing" = true;
    "privacy.resistFingerprinting.autoDeclineNoUserInputCanvasPrompts" = true;
    "security.OCSP.require" = true;
    "network.http.referer.XOriginPolicy" = 2;
    "media.autoplay.blocking_policy" = 2;
  };
}
