_: {
  programs.starship.settings = {
    add_newline = false;
    scan_timeout = 60;
    aws = {
      symbol = " ";
      expiration_symbol = " !";
    };
    azure.disabled = false;
    battery = {
      full_symbol = "󰁹";
      charging_symbol = "󰂅 ";
      discharging_symbol = "󱉝 ";
      unknown_symbol = "󰂑";
      empty_symbol = "󰂎";
      display = [
        {
          threshold = 20;
          charging_symbol = "󱊥 ";
          discharging_symbol = "󱊢";
          style = "bold yellow";
        }
        {
          threshold = 10;
          charging_symbol = "󱊤 ";
          discharging_symbol = "󱊡";
          style = "bold red";
        }
      ];
    };
    bun.symbol = " ";
    buf.symbol = "󰿘 ";
    c = {
      symbol = " ";
      style = "bold cyan";
    };
    cmake.symbol = "󰔷 ";
    cobol.symbol = "󱑠 ";
    cmd_duration.min_time = 1000;
    conda.symbol = "󱔎 ";
    container.symbol = " ";
    crystal = {
      symbol = " ";
      style = "bold white";
    };
    daml = {
      symbol = " ";
      style = "bold blue";
    };
    dart.symbol = " ";
    deno.symbol = " ";
    directory = {
      truncation_length = 5;
      read_only = "  ";
      read_only_style = "bold red";
    };
    docker_context.symbol = " ";
    dotnet = {
      format = "via [$symbol($version )($tfm )]($style)";
      style = "bold purple";
    };
    elixir.symbol = " ";
    elm.symbol = " ";
    fennel.symbol = " ";
    fossil_branch.style = "bold cyan";
    gcloud.symbol = " ";
    git_commit = {
      commit_hash_length = 8;
      tag_symbol = " 󰓹 ";
      format = "[\\($hash($tag)\\)]($style) ";
      tag_disabled = false;
    };
    git_state = {
      am = "APPLY-MAILBOX";
      am_or_rebase = "APPLY-MAILBOX/REBASING";
    };
    git_status = {
      conflicted = ''=''${count} '';
      ahead = ''⇡''${count} '';
      behind = ''⇣''${count} '';
      diverged = ''⇡''${ahead_count} ⇣''${behind_count} '';
      untracked = ''?''${count} '';
      stashed = ''\$''${count} '';
      modified = ''!''${count} '';
      staged = ''+''${count} '';
      renamed = ''»''${count} '';
      deleted = ''✘''${count} '';
      format = ''([$all_status$ahead_behind]($style))'';
    };
    gleam = {
      symbol = " ";
      style = "bold purple";
    };
    golang.symbol = " ";
    guix_shell.symbol = " ";
    gradle.symbol = " ";
    haskell.symbol = " ";
    haxe = {
      symbol = " ";
      style = "bold yellow";
    };
    helm.symbol = "󰠳 ";
    hostname.ssh_symbol = "SSH ";
    java = {
      symbol = " ";
      style = "bold red";
    };
    jobs.style = "bold yellow";
    julia = {
      symbol = " ";
      style = "dimmed green bold";
    };
    kotlin.symbol = " ";
    kubernetes = {
      symbol = "󱃾 ";
      style = "blue bold";
      disabled = false;
    };
    lua.symbol = " ";
    meson.symbol = "󰕣 ";
    nodejs.detect_files = [
      "package.json"
      ".node-version"
      ".nvmrc"
      "!bunfig.toml"
      "!bun.lockb"
      "!deno.json"
      "!deno.jsonc"
    ];
    hg_branch = {
      symbol = " ";
      disabled = false;
    };
    nats = {
      symbol = "󱀏 ";
      style = "cyan";
    };
    nim = {
      symbol = " ";
      style = "bold dimmed yellow";
    };
    nix_shell = {
      symbol = " ";
      style = "bold cyan";
    };
    ocaml.symbol = " ";
    opa = {
      symbol = "󱅧 ";
      style = "bold white";
    };
    openstack = {
      symbol = " ";
      style = "bold red";
    };
    package = {
      symbol = " ";
      style = "bold green";
      display_private = true;
    };
    perl = {
      symbol = " ";
      style = "bold purple";
    };
    php = {
      symbol = " ";
      style = "bold purple";
    };
    pijul_channel = {
      symbol = " ";
      style = "bold white";
      disabled = false;
    };
    pulumi = {
      symbol = " ";
      style = "bold purple";
    };
    python.symbol = "󰌠 ";
    quarto.style = "bold blue";
    rlang.symbol = "󰟔 ";
    raku = {
      symbol = " ";
      style = "bold yellow";
    };
    red.symbol = "󱥒 ";
    ruby = {
      symbol = "󰴭 ";
      style = "dimmed bold red";
      detect_files = [
        "Gemfile"
        ".ruby-version"
        "Rakefile"
      ];
    };
    rust.symbol = " ";
    scala = {
      symbol = " ";
      style = "dimmed bold red";
    };
    singularity.disabled = true;
    solidity.symbol = " ";
    spack.symbol = " ";
    status = {
      format = "[$status( \\(SIG$signal_name\\))]($style) ";
      disabled = false;
    };
    swift = {
      symbol = "󰛥 ";
      style = "bold red";
    };
    terraform = {
      symbol = " ";
      style = "bold purple";
      format = "via [$symbol$version\\($workspace\\)]($style) ";
    };
    typst = {
      symbol = " ";
      style = "bold bright-cyan";
    };
    vagrant.symbol = " ";
    vlang.symbol = " ";
  };
}
