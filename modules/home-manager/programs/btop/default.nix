_: {
  programs.btop.settings = {
    color_theme = "nord";
    theme_background = false;
    vim_keys = true;
    clock_format = "%X";
  };
}
