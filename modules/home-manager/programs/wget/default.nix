{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.programs.wget;

  wgetrcPath = "${config.xdg.configHome}/wgetrc";
in
with lib;
{
  options.programs.wget.enable = mkEnableOption "wget";

  config = mkIf cfg.enable {
    home.packages = [ pkgs.wget ];

    # Don't set this by default on <misc/xdg> module, as wget checks
    # for the existence of this file on startup.
    home.sessionVariables.WGETRC = wgetrcPath;

    # It's rare for anyone to change the default wget's settings,
    # so force only hsts-file value here.
    home.file.${wgetrcPath}.text = ''
      hsts-file = ${config.xdg.stateHome}/wget-hsts
    '';
  };
}
