{
  config,
  pkgs,
  lib,
  ...
}:
with lib;
mkMerge [
  {
    programs.emacs.package = with pkgs; if stdenv.isDarwin then emacs30 else emacs30-pgtk;

    home.sessionVariables = {
      DOOMLOCALDIR = "${config.xdg.dataHome}/doom";
      DOOMPROFILELOADFILE = "${config.xdg.dataHome}/doom/profiles/load.el";
    };
  }

  (mkIf config.programs.emacs.enable {
    xdg.configFile = {
      "emacs".source = pkgs.fetchFromGitHub {
        owner = "doomemacs";
        repo = "doomemacs";
        rev = "56ce6cc284e8f4dd0cb0704dde6694a1b8e500ed";
        hash = "sha256-dq3ZxCT4GIt14+DjCK1qmpxht0CHJ7cB6ha8bcPzfqQ=";
      };

      "doom" = {
        source = ./doom;
        recursive = true;
      };

      "doom/user.el".text =
        let
          cfg = config.programs.git;
        in
        ''
          (setq user-full-name "${optionalString (cfg.userName != null) cfg.userName}"
                user-mail-address "${optionalString (cfg.userEmail != null) cfg.userEmail}")
        '';
    };
  })
]
