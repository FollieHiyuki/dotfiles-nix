#+TITLE: RSS Feeds

* Reddit :elfeed:reddit:
** Unix :unix:
Linux and BSD related subreddit.
*** [[https://www.reddit.com/r/linux/new.rss][r/linux]] :linux:
*** [[https://www.reddit.com/r/linuxquestions/new.rss][r/linuxquestions]] :linux:
*** [[https://www.reddit.com/r/openbsd/new.rss][r/openbsd]] :bsd:
*** [[https://www.reddit.com/r/freebsd/new.rss][r/freebsd]] :bsd:
*** [[https://www.reddit.com/r/Gentoo/new.rss][r/Gentoo]] :linux:
*** [[https://www.reddit.com/r/archlinux/new.rss][r/archlinux]] :linux:
*** [[https://www.reddit.com/r/voidlinux/new.rss][r/voidlinux]] :linux:
*** [[https://www.reddit.com/r/NixOS/new.rss][r/NixOS]] :linux:
*** [[https://www.reddit.com/r/bedrocklinux/new.rss][r/bedrocklinux]] :linux:
*** [[https://www.reddit.com/r/Fedora/new.rss][r/Fedora]] :linux:
*** [[https://www.reddit.com/r/kde/new.rss][r/kde]]
*** [[https://www.reddit.com/r/commandline/new.rss][r/commandline]]
** Privacy :privacy:
*** [[https://www.reddit.com/r/privacy/new.rss][r/privacy]]
*** [[https://www.reddit.com/r/TOR/new.rss][r/TOR]]
*** [[https://www.reddit.com/r/Monero/new.rss][r/Monero]]
** Fluff :fluff:
Random interesting subreddit
*** [[https://www.reddit.com/r/unixporn/new.rss][r/unixporn]]
*** [[https://www.reddit.com/r/FirefoxCSS/new.rss][r/FirefoxCSS]]
*** [[https://www.reddit.com/r/startpages/new.rss][r/startpages]]
*** [[https://www.reddit.com/r/wallpaper/new.rss][r/wallpaper]]
*** [[https://www.reddit.com/r/Animewallpaper/new.rss][r/Animewallpaper]]
*** [[https://www.reddit.com/r/AnimeWallpapersSFW/new.rss][r/AnimeWallpapersSFW]]
*** [[https://www.reddit.com/r/awwnime/new.rss][r/awwnime]]
*** [[https://www.reddit.com/r/Lolirefugees/new.rss][r/Lolirefugees]]
*** [[https://www.reddit.com/r/AnimeGirlsInKimonos/new.rss][r/AnimeGirlsInKimonos]]
*** [[https://www.reddit.com/r/fatestaynight/new.rss][r/fatestaynight]]
*** [[https://www.reddit.com/r/Saber/new.rss][r/Saber]]
*** [[https://www.reddit.com/r/OneTrueTohsaka/new.rss][r/OneTrueTohsaka]]
*** [[https://www.reddit.com/r/formalwaifus/new.rss][r/formalwaifus]]
*** [[https://www.reddit.com/r/LightNovels/new.rss][r/LightNovels]]
*** [[https://www.reddit.com/r/PixelArt/new.rss][r/PixelArt]]
*** [[https://www.reddit.com/r/riprequests/new.rss][r/riprequests]]
*** [[https://www.reddit.com/r/dllinks/new.rss][r/dllinks]]
** Programming :programming:
*** [[https://www.reddit.com/r/learnprogramming/new.rss][r/learnprogramming]]
*** [[https://www.reddit.com/r/programming/new.rss][r/programming]]
*** [[https://www.reddit.com/r/coding/new.rss][r/coding]]
* News :elfeed:news:
** [[https://hnrss.org/frontpage][Hacker News]]
** [[https://lwn.net/headlines/rss][LWN.net]]
** [[https://thenewstack.io/blog/feed/][TheNewStack]]
** [[https://www.phoronix.com/rss.php][Phoronix]]
** CVE :security:
*** [[https://nvd.nist.gov/feeds/xml/cve/misc/nvd-rss.xml][8-day vulnerabilities]]
*** [[https://nvd.nist.gov/feeds/xml/cve/misc/nvd-rss-analyzed.xml][8-day vulnerabilities (info)]]
* Videos :elfeed:
** Youtube :youtube:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCUyeluBRhGPCW4rPe_UvBZQ][@ThePrimeTimeagen]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCOk-gHyjcWZNj3Br4oxwh0A][@TechnoTim]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC6nSFpj9HTCZ5t-N3Rm3-HA][@Vsauce]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCoxcjq-8xIDTYp3uz647V5A][@numberphile]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCBa659QWEk1AI4Tg--mrJ2A][@TomScottGo]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCEBb1b_L6zDS3xTUrIALZOw][@mitocw]] :programming:
MIT OpenCourseWare
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCcabW7890RKJzL968QWEykA][@cs50]] :programming:
Havard CS50
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC8butISFwT-Wl7EV0hUK0BQ][@freecodecamp]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCDiFRMQWpcp8_KD4vwIVicw][@emergencyawesome]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCjQK8kV4ik7p_605z8nTEgw][@xenotimeyt]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCbKWv2x9t6u8yZoB3KcPtnw][@alanbecker]]
Cool stick figure animation videos
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_WA7w7TmccwGEiyRjch2Nw][@phoebeyutbt]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCMnULQ6F6kLDAHxofDWIbrw][@PirateSoftware]] :gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC9zY_E8mcAo_Oq772LEZq8Q][@The_FirstTake]] :music:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC9k_g61pErlVg39Ol5H3uBg][@anormie8549]] :fluff:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCQeRaTukNYft1_6AZPACnog][@AsmonTV]] :gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_UwerUx8bGApFn_oIAuilw][@xLetalis]] :gaming:
Videos solely about Witcher 3 easter eggs and stuff
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCyHJ94JzwY92NsBVzJ2aE3Q][@econYT]] :media:
Videos about countries' economy
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC8fDTDebMOF9lF8nD7b0EZw][@jinnytty_]] :streamer:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCqVEHtQoXHmUCfJ-9smpTSg][@answerinprogress]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_Lnb8ZHqqgLbp-7hltuT9w][@CNAInsider]] :media:
Asia-focused media stories
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCZRoNJu1OszFqABP8AuJIuw][@LogicallyAnswered]] :media:
Videos about economics of tech and social media
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCx0L2ZdYfiq-tsAXb8IXpQg][@JustWrite]] :media:
Videos essays on movies, literature and writing
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UClJ0zw1TXKecGnygRZA3dXQ][@MrCompSciGuy]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCS0N5baNlQWJCUrhCEo8WlA][@BenEater]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC6biysICWOJ-C3P4Tyeggzg][@LowLevelTV]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCmyGZ0689ODyReHw3rsKLtQ][@michael_tunnell]] :news:linux:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC33pBiUW51-InqrLd0BmhMQ][@mkdevme]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC5U7mHlhP6s6478wd7ZvnhA][@Trafotin]] :media:programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCfGmaA-nXPryTfimsnkLieQ][@ChibiReviews]] :media:fluff:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCAxCk_CU0lnz6vJyslY9-Tw][@CrashAdams]] :music:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC-mTIBh__DzAqW495JHXy5A][@TheCodingGopher]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCSOfUqPoqpp5aWDDnAyv94g][@JVKE]] :music:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCrlZs71h3mTR45FgQNINfrg][@mathemaniac]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCdN4aXTrHAtfgbVG9HjBmxQ][@KeyAndPeele]] :media:humor:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCDsO-0Yo5zpJk575nKXgMVA][@rocketjump]] :media:humor:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCV0qA-eDDICsRR9rPcnG7tw][@jomakaze]] :programming:humor:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCSpFnDQr88xCZ80N-X7t0nQ][@CorridorCrew]] :media:design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCP0_k4INXrwPS6HhIyYqsTg][@StevenHe]] :humor:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCAzKFALPuF_EPe-AEI0WFFw][@twosetviolin]] :humor:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC477Kvszl9JivqOxN1dFgPQ][@IronPineapple]] :gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCCCEbjGk8O5LtG6XgwW6XWg][@Ashleynguci]] :fluff:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCvqbFHwN-nwalWPjPUKpvTA][@cncf]] :programming:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCfX55Sx5hEFjoC3cNs6mCUQ][@LinuxfoundationOrg]] :programming:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCWQaM7SpSECp9FELz-cHzuQ][@dreamsofcode]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_mYaQAE6-71rjSN6CeCA-g][@NeetCode]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCrqM0Ym_NbK1fqeQG2VIohg][@TsodingDaily]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC1Zfv1Zrp1q5lKgBomzOyCA][@MelkeyDev]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCXzw-OdotBUcNA9yhuYQBwA][@awesome-coding]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCZgt6AzoyjslHTC9dz0UoTw][@ByteByteGo]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC9x0AN7BWHpCDHSm9NiJFJQ][@NetworkChuck]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC-tLyAaPbRZiYrOJxAGB7dQ][@PursuitofWonder]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCcf1t1tH-Pp5bmu4hbAytwA][@escaping.ordinary]] :media:
Visual summaries of books
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCsXVk37bltHxD1rDPwtNM8Q][@kurzgesagt]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCNvzD7Z-g64bPXxGzaQaa4g][@gameranxTV]] :media:gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCxxw51w9d_utTp0VNyyG9IA][@aini_]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCqpg8i6CryfkIw0uqytHsow][@elliotsangestevez]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCqJ-Xo29CKyLTjn6z2XwYAw][@GMTK]] :gaming:media:
Game Maker's Toolkit (mostly for GMTK Game Jam content)
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCWnPjmqvljcafA0z2U1fwKQ][@Confreaks]] :programming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCTuK9I5lHq8E971RMez3PHg][@BofangChang]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCm8EsftbfNzSiRHzc7I59KQ][@kevinfaang]] :programming:media:
Technology incidents
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_slDWTPHflhuZqbGc8u4yA][@georainbolt]] :fluff:
GeoGuessr
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCjFqcJQXGZ6T6sxyFB-5i6A][@everyframeapainting]] :media:
From https://www.youtube.com/watch?v=oz49vQwSoTE
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCweDKPSF65wRw5VHFUJYiow][@CuriousArchive]] :gaming:
https://www.youtube.com/watch?v=ZFBUFFr4GmQ
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCIu_88SjcpTY47e4SyyVbBA][@RESPRiT]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCKeB3X0Akxxo3VfG46D_Dfg][@yukopi_]] :music:
https://www.youtube.com/watch?v=D6DVTLvOupE
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC-4-Itv3ved9ZWkSso9VE8A][@Renzy_YT]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCSIvk78tK2TiviLQn4fSHaw][@upandatom]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCRexPboeVnvnuK0aXtHqu_Q][@RaymondCripps]] :gaming:programming:
Project Feline's game devlog
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCUpi4-7EkY1JPWl6e7MGUtQ][@RubberRoss]] :design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCvG04Y09q0HExnIjdgaqcDQ][@becausescience]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCtm_9HCgC16YmA96coY3K0Q][@Phanimations]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCP1zJMzxD_MgTq2yEsNlO4w][@DXFromYT]] :media:gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCIFtADcWrOApqrsinzPoOfA][@DestinySpace]] :science:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCHu2KNu6TtJ0p4hpSW7Yv7Q][@Jazza]] :design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCY6Ij8zOds0WJEeqCLOnqOQ][@AlexMeyersVids]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCXjmz8dFzRJZrZY8eFiXNUQ][@Nerdstalgic]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCirpdr999dsaIhjaMuQ0S8g][@Sir.W]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC0mq0Ht7LZzaU-IH0PO6O6A][@redesignuxui]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCCDwHndN_0rTs4iklHOm9GQ][@thethriftytypewriter]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCQa8Hl88f4r12pljEYgmXgw][@NCUTcinematicproduction]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC4tvYotdETO_fG4V_Vpa3yA][@ItsWRMS]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCM55RjebjSevVWPnj2LEymw][@viyaura]] :design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC_iUeUzozCHEReJ-shKcCYA][@InternetAnarchist]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCSJPFQdZwrOutnmSFYtbstA][@TheCriticalDrinker]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCul6aD7dmSMP5CHC5GVgn3w][@thejessegrant]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC0SrgAKjftBuzHMfRZSjt7g][@lancelloti.]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCxbvyf6VoSUD9NgvJ2f9NEg][@ArcherGreen]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCCQhthkDN-KiAld7-i6Iaew][@josh_from_xboxlive]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCLSCddBxs40Hl8MXlZ7RmJQ][@bri.n.a]] :design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCHmDySLF9yQVC1oTwZKWtSQ][@AlexWebb101]] :media:gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC4OWLYVuwj55RzgmBbY2afg][@NovemberHotel]] :media:gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCcvfHa-GHSOHFAjU0-Ie57A][@AdamSomething]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCCNFCLWh1AA-qbyOyp8jlZg][@BritMonkey]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCMG5uQag6BoG4w4rH7iGt4w][@AlexBoucher]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCFOfgEQLHpPuVNP_m-XldTg][@FilmStack]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UClrSwyZsTOqfTkN7YkCKo5Q][@MingLLC]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCCA75dU6oaqia36HHrVveaQ][@SceneItReviews]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCUyvQV2JsICeLZP4c_h40kA][@ThomasFlight]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC3d64X0YAFQHxAOXN49B10w][@mattwith4ts]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCSHtaUm-FjUps090S7crO4Q][@bernadettebanner]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCW_c5PzkvxMaBxqwjITW6-A][@ModernGurlz]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCLuI5IP4nBr82VxClxp7Teg][@friendlyspaceninja]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCL8h3ri2WN_-IbviBlWtUcQ][@CinemaWins]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCNfdeIOg0IbUVDkRlEnJGCA][@artatmidnight]] :media:design:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCafMyiPeOQs90AtRc7EK1Sg][@ZellyJP]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCw_fRfvZPfYJwYP-vlb7Ftg][@BeyondDrawings]] :media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC-N6t0mwTMtdKYRCERJObsw][@MelTorrefranca]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCZXW8E1__d5tZb-wLFOt8TQ][@bogxd]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UClZbmi9JzfnB2CEb0fG8iew][@primalspace]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC2C_jShtL725hvbm1arSV9w][@CGPGrey]]
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC64UiPJwM_e9AqAd7RiD7JA][@TodayIFoundOut]] :science:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCWFNUk2LHUQdRzQcJLYZmcg][@BringusStudios]] :media:gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UC2ksme4hP4Nx97ilFTQ0K0Q][@ThreatInteractive]] :gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCdbJP74-u0OH9uI6Qt3-p4g][@ughneyko]] :gaming:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCyFtmcd8bx698Ls9eirgSAQ][@Hybred]] :gaming:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCTfXFm3IY3wU6k5a3rctabA][@radical_cap]] :gaming:media:
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCozaueNQBLfdlV8FbBxdxVQ][@RiftDivision]] :gaming:
Rift Division game's devlog
*** [[https://www.youtube.com/feeds/videos.xml?channel_id=UCi35azv931ZP2vmJ9t6sL2Q][@ThatCowBloke]]
** Odysee :odysee:
*** [[https://odysee.com/$/rss/@VikingAlgeria:5][@VikingAlgeria]] :fluff:
Gifs with sound
*** [[https://odysee.com/$/rss/@DistroTube:2][@DistroTube]] :linux:
*** [[https://odysee.com/$/rss/@BrodieRobertson:5][@BrodieRobertson]] :linux:
*** [[https://odysee.com/$/rss/@AlphaNerd:8][@AlphaNerd]] :linux:
Mental Outlaw on Youtube
*** [[https://odysee.com/$/rss/@bW:9][@bW]] :linux:
baby WOGUE on Youtube
*** [[https://odysee.com/$/rss/@SystemCrafters:e][@SystemCrafters]] :linux:programming:
*** [[https://odysee.com/$/rss/@teej_dv:d][@teej_dv]] :programming:
*** [[https://odysee.com/$/rss/@veritasium:f][@veritasium]] :science:
*** [[https://odysee.com/$/rss/@3Blue1Brown:b][@3Blue1Brown]] :science:
*** [[https://odysee.com/$/rss/@fireship:6][@fireship]] :programming:media:
* Blogs :elfeed:blog:
** Engineering :company:
I'll miss https://www.linkedin.com/blog/engineering as it doesn't have RSS feed anymore.
Ref: https://github.com/kilimchoi/engineering-blogs/blob/master/engineering_blogs.opml
*** [[https://aws.amazon.com/blogs/aws/feed/][AWS Blog]]
*** [[https://blog.cloudflare.com/rss][Cloudflare Blog]]
*** [[https://engineering.fb.com/feed/][Facebook Engineer Blog]]
*** [[https://github.blog/engineering.atom][GitHub Engineering Blog]]
*** [[https://about.gitlab.com/atom.xml][GitLab Blog]]
*** [[https://www.hashicorp.com/blog/feed.xml][Hashicorp Blog]]
*** [[https://devblogs.microsoft.com/engineering-at-microsoft/feed/][Microsoft Engineering Blog]]
*** [[https://netflixtechblog.com/feed][Netflix Technology Blog]]
*** [[https://slack.engineering/rss][Slack Engineering Blog]]
*** [[https://engineering.atspotify.com/feed][Spotify Engineering Blog]]
*** [[https://stripe.com/blog/feed.rss][Stripe Blog]]
*** [[https://tailscale.com/blog/index.xml][Tailscale Blog]]
*** [[https://www.uber.com/blog/engineering/rss/][Uber Engineering Blog]]
*** [[https://pigsty.io/blog/db/index.xml][Pigsty Blog]]
A cool perspective about PostgreSQL. Basically just a blog of Ruohang Feng, the author of =pigsty=.
** People :people:
*** [[https://pixelde.su/blog/posts.rss][Andreas Nedbal]]
*** [[https://drewdevault.com/blog/index.xml][Drew Devault]]
*** [[https://emersion.fr/blog/atom.xml][Simon Ser]]
*** [[https://www.paritybit.ca/feed.xml][Jake Bauer]]
*** [[https://reversed.top/feed.xml][xaizek]]
*** [[https://whynothugo.nl/posts.xml][Hugo Osvaldo Barrera]]
*** [[https://jvns.ca/atom.xml][Julia Evans]]
*** [[https://samhh.com/rss.xml][Sam A. Horvath-Hunt]]
*** [[https://sachachua.com/blog/feed][Sacha CHua]]
*** [[https://www.davidrevoy.com/feed/rss][David Revoy]]
*** [[https://codemadness.org/feeds.html][CodeMadness]]
*** [[https://dudemanguy.github.io/blog/rss.xml][Dudemanguy]]
*** [[https://purplesyringa.moe/blog/feed.rss][purplesyringa]]
*** [[https://zserge.com/rss.xml][Serge Zaitsev]]
*** [[https://strongly-typed-thoughts.net/blog/feed][Dimitri Sabadie]]
*** [[https://alternativebit.fr/posts/index.xml][Félix]]
*** [[https://peter.eisentraut.org/feed.xml][Peter Eisentraut]]
* Podcasts :elfeed:podcast:
** [[https://tuxdigital.com/feed/thisweekinlinux-mp3][This Week in Linux]] :news:linux:
** [[https://www.arresteddevops.com/episode/index.xml][Arrested DevOps]]
** [[https://feeds.pacific-content.com/commandlineheroes][Command Line Heroes]]
** [[https://selfhosted.show/rss][Self-Hosted]]
** [[https://sudo.show/rss][Sudo Show]]
** [[https://gotime.fm/rss][Go Time: Golang, Software Engineering]]
** [[https://golangshow.com/index.xml][GolangShow Podcast (Russian)]]
** [[https://newrustacean.com/feed.xml][New Rustacean]]
** [[https://nodiagnosticrequired.tv/rss/][No Diagnostic Required]]
** [[https://cppcast.com/episode/index.xml][CppCast]]
** [[https://www.twoscomplement.org/podcast/feed.xml][Two's Complement]]
** [[https://realpython.com/podcasts/rpp/feed][The Real Python Podcast]]
** [[https://talkpython.fm/episodes/rss][Talk Python to me]]
** [[https://www.pythonpodcast.com/feed/mp3/][The Python Podcast.__init__]]
** [[https://www.bikeshed.fm/rss][The Bike Shed]]
** [[http://feeds.libsyn.com/110110/rss][The freeCodeCamp Podcast]]
