{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.programs.ruby;
in
with lib;
{
  options.programs.ruby = {
    enable = mkEnableOption "ruby";

    package = mkPackageOption pkgs "ruby" { };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];

    home.sessionPath = [
      "$(${cfg.package}/bin/gem env user_gemhome)/bin"
    ];
  };
}
