{
  config,
  lib,
  pkgs,
  ...
}:
let
  forgit = pkgs.fetchFromGitHub {
    owner = "wfxr";
    repo = "forgit";
    rev = "755b3dd9f85c044a3bd76059b24b1ad63d5c0617";
    hash = "sha256-tZFGQirm7dCHXGrnAB5qynVzWEMKnV/6Kfmja/3Os20=";
  };
in
with lib;
{
  options.programs.git-forgit.enable = mkEnableOption "git-forgit";

  config = lib.mkIf config.programs.git-forgit.enable {
    home.file."${config.xdg.binHome}/git-forgit".source = "${forgit}/bin/git-forgit";

    # Add shell completions
    xdg.configFile."fish/completions/git-forgit.fish" = lib.mkIf config.programs.fish.enable {
      source = "${forgit}/completions/git-forgit.fish";
    };
    programs.zsh.initExtraBeforeCompInit = lib.mkIf config.programs.zsh.enable ''
      fpath+=(${forgit}/completions)
    '';
  };
}
