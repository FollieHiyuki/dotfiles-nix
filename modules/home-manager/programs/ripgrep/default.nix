_: {
  programs.ripgrep.arguments = [
    "--hidden"
    "--smart-case"
    "--glob=!.git/*"
    "--glob=!node_modules"
  ];
}
