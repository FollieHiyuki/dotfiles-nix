{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.programs.ghq;
in
with lib;
{
  options.programs.ghq = {
    enable = mkEnableOption "ghq";

    root = mkOption {
      type = types.path;
      defaultText = "~/Code";
      apply = builtins.toString;
      description = ''
        Path to ghq's root, storing all cloned repositories.
      '';
    };
  };

  config = mkIf cfg.enable {
    programs.ghq.root = mkDefault "${config.home.homeDirectory}/Code";

    home.packages = [ pkgs.ghq ];
    programs.git.extraConfig.ghq.root = cfg.root;

    programs.zsh.initExtra = ''
      ghq_cd() {
        local proj="$(ghq list --full-path | fzf \
          --no-multi \
          --prompt 'Project: ' \
          --preview 'lsd -1FAL --group-dirs first --icon always --color always {}' \
          )"
        [ -n "$proj" ] && cd "$proj"
      }

      ghq_rm() {
        local projects="$(ghq list --exact | fzf --multi --prompt 'Project: ')"

        if [ -n "$projects" ]; then
          for proj in ''${(ps:\n:)projects}; do
            local proj_dir="$(ghq list --full-path "$proj")"
            ghq rm "$proj" && { rmdir -p "$(dirname "$proj_dir")" 2>/dev/null || true }
          done
        fi
      }
    '';

    programs.fish.functions = {
      ghq_cd = {
        description = "Interactive selection and cd into a project";
        body = ''
          set -l proj (ghq list --full-path | fzf \
            --no-multi \
            --prompt "Project: " \
            --preview 'lsd -1FAL --group-dirs first --icon always --color always {}' \
            )
          if test -n "$proj"
            cd -- "$proj"
          end
        '';
      };

      ghq_rm = {
        description = "Interactive selection and recursively delete repositories";
        body = ''
          set -l projects (ghq list --exact | fzf --multi --prompt "Project: ")

          if test -n "$projects"
            for proj in $projects
              set -l proj_dir (ghq list --full-path "$proj")
              ghq rm "$proj"; and begin
                rmdir -p (dirname "$proj_dir") 2>/dev/null; or true
              end
            end
          end
        '';
      };
    };
  };
}
