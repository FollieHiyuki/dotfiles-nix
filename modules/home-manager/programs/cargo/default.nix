{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
{
  options.programs.cargo.enable = mkEnableOption "cargo";

  config = mkIf config.programs.cargo.enable {
    home.sessionPath = [ "${config.home.sessionVariables.CARGO_HOME}/bin" ];

    # I use clangd, so putting clang here is more convenient than gcc :)
    home.packages = with pkgs; [
      clang
      cargo
      rustc
    ];
  };
}
