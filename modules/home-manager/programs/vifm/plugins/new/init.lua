-- Yanked from https://github.com/vifm/vifm/issues/972#issuecomment-1872981190
-- with modifications to use new Vifm's Lua APIs.

--[[

Provides :new command to create files / directories recursively.

Usage examples:
    - Create a new file named test_file
        :new test_file
    - Create a new directory named test_dir
        :new test_dir/
    - Create multiple new files and directories
        :new a b/c/d e/f/

--]]

local function new(info)
  for _, path in ipairs(info.argv) do
    if vifm.exists(path) then
      vifm.errordialog('new', 'Path already exists: ' .. path)
      goto continue
    end

    local dir = string.match(path, '.+/')

    if dir then
      if not vifm.fs.mkdir(dir, 'create') then
        vifm.errordialog('new', 'Failed to create directory: ' .. dir)
        goto continue
      end
    end

    if not (dir and #dir == #path) then
      if not vifm.fs.mkfile(path) then
        vifm.errordialog('new', 'Failed to create file: ' .. path)
      end
    end

    ::continue::
  end
end

local added = vifm.cmds.add {
  name = 'new',
  description = 'create files and directories',
  minargs = 1,
  maxargs = -1,
  handler = new,
}
if not added then
  vifm.sb.error('Failed to register :new')
end

return {}
