{
  config,
  pkgs,
  lib,
  ...
}:
with lib;
mkMerge [
  { programs.vifm.package = if pkgs.stdenv.isLinux then pkgs.vifm-full else pkgs.vifm; }

  (mkIf config.programs.vifm.enable {
    xdg.configFile = {
      "vifm/vifmrc".source = ./vifmrc;

      "vifm/colors" = {
        source = ./colors;
        recursive = true;
      };

      "vifm/plugins" = {
        source = ./plugins;
        recursive = true;
      };
    };
  })

  # libcurses-based applications rely on -direct terminfo to be able to set 24-bit colors.
  # Ref:
  # - https://github.com/vifm/vifm/issues/815
  # - https://github.com/vifm/vifm/issues/717#issuecomment-921926481
  # - https://gist.github.com/XVilka/8346728?permalink_comment_id=3113504#gistcomment-3113504
  (mkIf (config.programs.vifm.enable && config.programs.alacritty.enable) {
    programs.alacritty.settings.env.TERM = mkDefault "alacritty-direct";
    home.packages = [ pkgs.alacritty.terminfo ];
  })
  (mkIf (config.programs.vifm.enable && config.programs.foot.enable) {
    programs.foot.settings.main.term = mkDefault "foot-direct";
    home.packages = [ pkgs.foot.terminfo ];
  })
]
