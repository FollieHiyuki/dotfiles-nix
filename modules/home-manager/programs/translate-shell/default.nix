{
  config,
  lib,
  pkgs,
  ...
}:
{
  programs.translate-shell.settings = {
    identify = true;
    view = false;
    show-original = false;
    show-original-dictionary = true;
    indent = "2";
    hl = "en";
    tl = [
      "ja"
      "ru"
    ];
    engine = lib.mkDefault "google";
    browser = if pkgs.stdenv.isDarwin then "open" else "${pkgs.xdg-utils}/bin/xdg-open";
    theme = lib.mkDefault "happiness.trans";
    user-agent = config.home.userAgentString;
  };

  xdg.configFile."translate-shell/happiness.trans" = {
    inherit (config.programs.translate-shell) enable;

    source = pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/soimort/translate-shell/registry/themes/happiness.trans";
      hash = "sha256-xhiXogw8TFApErtHyUkpJhAiPyyTDaTvYSstNRSpBIw=";
    };
  };
}
