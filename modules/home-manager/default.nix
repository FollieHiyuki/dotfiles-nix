{ inputs, ... }:
{
  home-manager = {
    verbose = true;
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = {
      inherit inputs;
    };
    backupFileExtension = "old";

    sharedModules =
      with inputs;
      [
        sops-nix.homeManagerModules.sops
        nix-flatpak.homeManagerModules.nix-flatpak
        plasma-manager.homeManagerModules.plasma-manager
      ]
      ++ (self.lib.listModuleDirsRecursive ./.);
  };
}
