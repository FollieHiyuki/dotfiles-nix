{
  config,
  lib,
  pkgs,
  ...
}:
let
  inherit (config) home xdg;

  # Enforce the same age key location on all platforms
  ageKeyFile = "${xdg.configHome}/sops/age/keys.txt";

  # Ref: https://github.com/NixOS/nixpkgs/blob/nixos-unstable/nixos/modules/services/misc/guix/default.nix
  guixUserProfiles = [
    "${xdg.configHome}/guix/current"
    "${home.homeDirectory}/.guix-home/profile"
    "${home.homeDirectory}/.guix-profile"
  ];
in
with lib;
{
  # XDG_BIN_HOME is not set by default
  options.xdg.binHome = mkOption {
    type = types.path;
    defaultText = "~/.local/bin";
    apply = builtins.toString;
    description = ''
      Absolute path to directory storing user-specific executables.
    '';
  };

  config = mkMerge [
    {
      xdg.enable = true;
      home.preferXdgDirectories = true;

      # Ref: https://wiki.archlinux.org/title/XDG_Base_Directory
      home.sessionVariables = {
        MIX_XDG = "true";
        GHCUP_USE_XDG_DIRS = "true";
        _JAVA_OPTIONS = builtins.toString (
          mapAttrsToList (name: value: "-D${name}=${value}") {
            "java.util.prefs.userRoot" = "${xdg.configHome}/java";
            "javafx.cachedir" = "${xdg.cacheHome}/openjfx";
          }
        );

        ANSIBLE_GALAXY_CACHE_DIR = "${xdg.cacheHome}/ansible/galaxy_cache";
        ANSIBLE_LOCAL_TEMP = "${xdg.cacheHome}/ansible/tmp";
        APPTAINER_CACHEDIR = "${xdg.cacheHome}/apptainer";
        LINUXKIT_CACHE = "${xdg.cacheHome}/linuxkit";
        MAGEFILE_CACHE = "${xdg.cacheHome}/magefile";
        NPM_CONFIG_CACHE = "${xdg.cacheHome}/npm";
        PUPPETEER_CACHE_DIR = "${xdg.cacheHome}/puppeteer";
        TIMONI_CACHE_DIR = "${xdg.cacheHome}/timoni";
        TF_PLUGIN_CACHE_DIR = "${xdg.cacheHome}/terraform/plugin-cache";

        AWS_CONFIG_FILE = "${xdg.configHome}/aws/config";
        DEVPOD_CONFIG = "${xdg.configHome}/devpod/config.yaml";
        DOCKER_CONFIG = "${xdg.configHome}/docker";
        MOST_INITFILE = "${xdg.configHome}/mostrc";
        NPM_CONFIG_USERCONFIG = "${xdg.configHome}/npm/npmrc";
        NPM_CONFIG_INIT_MODULE = "${xdg.configHome}/npm/npm-init.js";

        ANDROID_HOME = "${xdg.dataHome}/android/sdk";
        ANDROID_USER_HOME = "${xdg.dataHome}/android";
        ANSIBLE_HOME = "${xdg.dataHome}/ansible";
        AZURE_CONFIG_DIR = "${xdg.dataHome}/azure";
        CARGO_HOME = "${xdg.dataHome}/cargo";
        CDKTF_HOME = "${xdg.dataHome}/terraform-cdk";
        DEVPOD_HOME = "${xdg.dataHome}/devpod";
        GOPATH = "${xdg.dataHome}/go";
        GRADLE_USER_HOME = "${xdg.dataHome}/gradle";
        KREW_ROOT = "${xdg.dataHome}/krew";
        MINIKUBE_HOME = "${xdg.dataHome}/minikube";
        NPM_CONFIG_PREFIX = "${xdg.dataHome}/npm";
        OPAMROOT = "${xdg.dataHome}/opam";
        PULUMI_HOME = "${xdg.dataHome}/pulumi";
        RUSTUP_HOME = "${xdg.dataHome}/rustup";
        STACK_ROOT = "${xdg.dataHome}/stack";

        SQLITE_HISTORY = "${xdg.stateHome}/sqlite/history";
        NODE_REPL_HISTORY = "${xdg.stateHome}/node_repl_history";
        NPM_CONFIG_LOGS_DIR = "${xdg.stateHome}/npm/logs";
      };

      sops.age.keyFile = ageKeyFile;
    }

    (mkIf pkgs.stdenv.isLinux {
      xdg.userDirs = {
        enable = true;
        createDirectories = true;
      };

      xdg.mimeApps.enable = true;

      home.sessionVariables = {
        GUIX_PROFILE = builtins.concatStringsSep ":" guixUserProfiles;
        GUIX_LOCPATH = makeSearchPath "lib/locale" guixUserProfiles;
        GUIX_EXTENSIONS_PATH = makeSearchPath "share/guix/extensions" guixUserProfiles;
      };
    })

    (mkIf pkgs.stdenv.isDarwin {
      # This is the default on Linux, so only set the variable on MacOS
      home.sessionVariables.SOPS_AGE_KEY_FILE = ageKeyFile;
    })

    # Default config for xdg.binHome
    (mkIf xdg.enable {
      xdg.binHome = mkDefault "${home.homeDirectory}/.local/bin";
      home.sessionVariables.XDG_BIN_HOME = xdg.binHome;
      systemd.user.sessionVariables.XDG_BIN_HOME = mkIf pkgs.stdenv.isLinux xdg.binHome;
    })
  ];
}
