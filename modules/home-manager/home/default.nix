{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.home.userAgentString = lib.mkOption {
    type = lib.types.str;
    default = "Mozilla/5.0 (Windows NT 10.0; rv:131.0) Gecko/20100101 Firefox/131.0";
    description = ''
      Common user agent string used by various CLI tools.
    '';
  };

  config = {
    programs.home-manager.enable = true;

    home.stateVersion = "24.11";

    # Link static data files into places
    home.file."${config.xdg.binHome}" = {
      source = ./local/bin;
      recursive = true;
    };

    home.shellAliases = {
      cp = "cp -vir";
      mv = "mv -vi";
      rm = "rm -vr";
      mkdir = "mkdir -vp";
    };

    home.sessionVariables = {
      # Common env
      PAGER = "${pkgs.less}/bin/less";
      MANPAGER = "${pkgs.less}/bin/less -R --use-color -Dd+b -Du+g -DS+kc";
      MANROFFOPT = "-P -c"; # this variable only affects man-db
      LESS = "-R -i -M --incsearch";
      LESSHISTFILE = "-";
      BROWSER = if pkgs.stdenv.isDarwin then "open" else "xdg-open";

      # Hashicorp telemetry
      CHECKPOINT_DISABLE = "true";
      DISABLE_VERSION_CHECK = "true";

      # atlas
      ATLAS_NO_UPDATE_NOTIFIER = "true";

      # aws-sam-cli
      SAM_CLI_TELEMETRY = 0;

      # azure-cli
      # FIXME: no way to disable azure-cli's auto-upgrade with env var
      # Ref: https://github.com/Azure/azure-cli/issues/19200
      AZURE_CORE_COLLECT_TELEMETRY = "false";
      AZURE_CORE_ERROR_RECOMMENDATION = "off";
      AZURE_CORE_SURVEY_MESSAGE = "false";

      # deno
      DENO_NO_UPDATE_CHECK = 1;

      # devpod
      DEVPOD_SKIP_VERSION_CHECK = "true";
      DEVPOD_DISABLE_TELEMETRY = "true";

      # driftctl
      DCTL_NO_VERSION_CHECK = "true";

      # gcloud
      CLOUDSDK_COMPONENT_MANAGER_DISABLE_UPDATE_CHECK = "true";
      CLOUDSDK_CORE_DISABLE_USAGE_REPORTING = "true";

      # cargo
      CARGO_REGISTRIES_CRATES_IO_PROTOCOL = "sparse";
      OPENSSL_NO_VENDOR = 1;

      # dotnet
      DOTNET_CLI_TELEMETRY_OPTOUT = "true";
      DOTNET_SKIP_FIRST_TIME_EXPERIENCE = "true";

      # go
      GOPROXY = "direct";
      GOSUMDB = "off";
      GOTOOLCHAIN = "local";

      # helmfile
      HELMFILE_UPGRADE_NOTICE_DISABLED = "true";
      HELMFILE_DISABLE_INSECURE_FEATURES = "true";
      HELMFILE_SKIP_INSECURE_TEMPLATE_FUNCTIONS = "true";

      # kics
      DISABLE_CRASH_REPORT = "false";

      # kopia
      KOPIA_CHECK_FOR_UPDATES = "false";
      KOPIA_PERSIST_CREDENTIALS_ON_CONNECT = "false";

      # kubeshark
      KUBESHARK_DISABLE_VERSION_CHECK = "true";

      # pnpm
      NPM_CONFIG_UPDATE_NOTIFIER = "false";

      # pulumi
      PULUMI_DISABLE_AUTOMATIC_PLUGIN_ACQUISITION = "true";
      PULUMI_ERROR_ON_DEPENDENCY_CYCLES = "true";
      PULUMI_ERROR_OUTPUT_STRING = "true";
      # PULUMI_IGNORE_AMBIENT_PLUGINS = "true";
      PULUMI_SKIP_UPDATE_CHECK = "true";

      # open-policy-agent
      OPA_TELEMETRY_SERVICE_URL = "http://0.0.0.0";

      # tilt.dev
      TILT_DISABLE_ANALYTICS = 1;

      # qsv
      QSV_PROGRESSBAR = "true";
      QSV_NO_UPDATE = "true";

      # usql
      # Don't output the logo. It wastes terminal space.
      USQL_TERM_GRAPHICS = "none";

      # regal
      REGAL_DISABLE_CHECK_VERSION = "true";

      # tabby LLM
      TABBY_DISABLE_USAGE_COLLECTION = 1;

      # turborepo
      TURBO_TELEMETRY_DISABLED = 1;

      # yarn-berry
      YARN_ENABLE_TELEMETRY = 0;

      # winglang
      WING_DISABLE_ANALYTICS = 1;

      # mage
      MAGEFILE_ENABLE_COLOR = "true";

      # skip chromium/electron download
      PUPPETEER_SKIP_DOWNLOAD = "true";
      ELECTRON_SKIP_BINARY_DOWNLOAD = 1;
    };

    home.sessionPath = [ "${config.xdg.binHome}" ];

    sops = {
      defaultSopsFile = ../secrets.yaml;
      defaultSopsFormat = "yaml";

      age.keyFile = lib.mkDefault "${config.xdg.configHome}/sops/age/keys.txt";
    };
  };
}
