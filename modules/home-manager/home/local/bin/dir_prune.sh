#!/usr/bin/env nix
#! nix shell nixpkgs#fd --command /bin/sh

# This script removes empty subdirectories recursively from
# a given directory.

[ $# -eq 0 ] && {
	>&2 echo "No arguments supplied"
	exit 1
}

for arg in "$@"; do
	if [ ! -d "$arg" ]; then
		>&2 printf "=> \033[1;31m%s\033[0m is not a directory.\n" "$arg"
		continue
	fi

	printf "=> Pruning directory \033[1;31m%s\033[0m\n" "$arg"
	for dir in $(fd --full-path "$arg" --type empty --type directory); do
		echo "+ $dir"
		rmdir -p "$dir" 2>/dev/null || true
	done
done
