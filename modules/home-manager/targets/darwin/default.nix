{
  inputs,
  osConfig,
  config,
  lib,
  pkgs,
  ...
}:
{
  disabledModules = [ "targets/darwin/linkapps.nix" ];

  config = lib.mkIf pkgs.stdenv.isDarwin {
    home.activation.linkApplications = lib.hm.dag.entryAfter [ "writeBoundary" ] (
      inputs.self.lib.aliasDarwinApps {
        apps = pkgs.buildEnv {
          name = "home-manager-applications";
          # FIXME: user packages from nix-darwin aren't copied/linked, so do it in home-manager instead
          # Ref: https://github.com/LnL7/nix-darwin/issues/139
          paths = config.home.packages ++ osConfig.users.users.${config.home.username}.packages;
          pathsToLink = "/Applications";
        };
        targetDir = "$HOME/Applications/Nix Apps";
      }
    );

    targets.darwin = {
      search = "DuckDuckGo";
      currentHostDefaults."com.apple.controlcenter".BatteryShowPercentage = true;
      defaults."com.apple.desktopservices" = {
        DSDontWriteNetworkStores = true;
        DSDontWriteUSBStores = true;
      };
    };
  };
}
