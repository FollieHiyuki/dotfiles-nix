{ config, ... }:
{
  services.gpg-agent = {
    inherit (config.programs.gpg) enable;

    enableSshSupport = true;
    defaultCacheTtl = 7200;
    maxCacheTtl = 14400;
    defaultCacheTtlSsh = 7200;
    maxCacheTtlSsh = 14400;

    extraConfig = ''
      no-allow-external-cache
    '';
  };
}
