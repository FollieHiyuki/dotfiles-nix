{ config, ... }:
{
  services.emacs = {
    inherit (config.programs.emacs) enable;

    client.enable = true;
    startWithUserSession = "graphical";
  };
}
