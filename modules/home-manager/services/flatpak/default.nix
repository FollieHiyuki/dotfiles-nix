{ pkgs, lib, ... }:
lib.mkIf pkgs.stdenv.isLinux {
  services.flatpak = {
    remotes = [
      {
        name = "flathub";
        location = "https://dl.flathub.org/repo/flathub.flatpakrepo";
      }
      {
        name = "elementary";
        location = "https://flatpak.elementary.io/repo.flatpakrepo";
      }
    ];

    update.auto = {
      enable = true;
      onCalendar = "weekly";
    };

    uninstallUnmanaged = true;
  };
}
