{ config, lib, ... }:
{
  options.programs.amethyst.enable = lib.mkEnableOption "amethyst";

  # NOTE: allow the app to control the computer in Privacy & Security > Accessibility
  config = lib.mkIf config.programs.amethyst.enable {
    homebrew.casks = [ "amethyst" ];
    system.defaults.dock.mru-spaces = false;
  };
}
