{
  inputs,
  config,
  lib,
  ...
}:
{
  system.activationScripts.applications.text = lib.mkForce (
    inputs.self.lib.aliasDarwinApps {
      apps = config.system.build.applications;
      targetDir = "/Applications/Nix Apps";
    }
  );
}
