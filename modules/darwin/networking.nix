{ config, lib, ... }:
lib.mkMerge [
  {
    networking.knownNetworkServices = [
      "Wi-Fi"
    ];
  }

  (lib.mkIf config.services.dnscrypt-proxy.enable {
    networking.dns = [
      "127.0.0.1"
      "::1"
    ];
  })
]
