{ inputs, ... }:
{
  imports = (inputs.self.lib.listModules ./.) ++ [ ../nixos/common ];

  # Taps are not managed by nix-homebrew due to https://github.com/zhaofengli/nix-homebrew/issues/5
  nix-homebrew = {
    enable = true;

    # No telemetry please, HomeBrew!
    extraEnv.HOMEBREW_NO_ANALYTICS = "1";
  };
  homebrew = {
    enable = true;
    onActivation.cleanup = "zap";
    global.autoUpdate = false;
  };

  # vim exists by default on Darwin. It's better than the default value `nano` :)
  environment.variables.EDITOR = "vim";

  system.defaults = {
    NSGlobalDomain = {
      AppleShowScrollBars = "WhenScrolling";
      NSAutomaticCapitalizationEnabled = false;
      NSAutomaticSpellingCorrectionEnabled = false;
      NSDocumentSaveNewDocumentsToCloud = false;
      "com.apple.mouse.tapBehavior" = 1;
    };
    dock = {
      autohide = true;
      orientation = "bottom";
      show-recents = false;
      showhidden = true;
      wvous-bl-corner = 11; # Launchpad
      wvous-tl-corner = 2; # Mission Control
    };
    finder = {
      AppleShowAllFiles = true;
      AppleShowAllExtensions = true;
      CreateDesktop = false;
      ShowPathbar = true;
    };
    trackpad.Clicking = true;
  };
}
