{ config, lib, ... }:
{
  # NOTE: keep this in sync with home-manager's config.xdg.stateHome value (there is no XDG_STATE_HOME in the global environment).
  # 700 is used so it appears before $HOME/.nix-profile having mkOrder 800.
  # Ref: https://github.com/LnL7/nix-darwin/issues/943
  environment.profiles = lib.mkOrder 700 [ "$HOME/.local/state/nix/profile" ];

  services.nix-daemon.tempDir = config.nix.settings.build-dir;

  nix.nrBuildUsers = lib.mkDefault 32;

  nix.daemonIOLowPriority = lib.mkDefault true;

  nix.gc = {
    automatic = true;
    options = "--delete-older-than 14d";
    interval.Day = 7;
  };
}
