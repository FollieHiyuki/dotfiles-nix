{ lib, pkgs, ... }:
let
  optionName = if pkgs.stdenv.isDarwin then "dnscrypt-proxy" else "dnscrypt-proxy2";
in
{
  services.${optionName}.settings = {
    server_names = lib.mkDefault [
      "wikimedia"
      "wikimedia-ipv6"
      "mullvad-doh"
      "njalla-doh"
      "cleanbrowsing-security"
      "cleanbrowsing-security-ipv6"
      "cleanbrowsing-security-doh"
    ];
    listen_addresses = [
      "127.0.0.1:53"
      "[::1]:53"
    ];
    ipv4_servers = true;
    ipv6_servers = true;
    dnscrypt_servers = true;
    doh_servers = true;
    odoh_servers = true;
    require_dnssec = true;
    require_nolog = true;
    require_nofilter = false;
    http3 = true;
    use_syslog = true;
    blocked_query_response = "refused";
    dnscrypt_ephemeral_keys = true;
    tls_disable_session_tickets = true;
    ignore_system_dns = true;
    bootstrap_resolvers = lib.mkDefault [
      "1.1.1.1:53"
      "9.9.9.9:53"
    ];
    netprobe_address = lib.mkDefault "1.1.1.1:53";
    block_ipv6 = false;
    block_unqualified = true;
    block_undelegated = true;
    cache_size = 4096;
    cache_min_ttl = 2400;
    cache_max_ttl = 86400;
    blocked_ips.blocked_ips_file = ./blocked-ips.txt;
    anonymized_dns = {
      skip_incompatible = true;
      direct_cert_fallback = false;
    };

    # Ref: https://github.com/DNSCrypt/dnscrypt-resolvers/tree/master/v3
    sources =
      builtins.mapAttrs
        (
          name: value:
          {
            minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
            cache_file = name + ".md";
            refresh_delay = 72;
            prefix = "";
          }
          // value
        )
        {
          public-resolvers.urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
            "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
          ];

          relays.urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md"
            "https://download.dnscrypt.info/resolvers-list/v3/relays.md"
          ];

          opennic.urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/opennic.md"
            "https://download.dnscrypt.info/resolvers-list/v3/opennic.md"
          ];

          odoh-servers.urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/odoh-servers.md"
            "https://download.dnscrypt.info/resolvers-list/v3/odoh-servers.md"
          ];

          odoh-relays.urls = [
            "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/odoh-relays.md"
            "https://download.dnscrypt.info/resolvers-list/v3/odoh-relays.md"
          ];
        };
  };
}
