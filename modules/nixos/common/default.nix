{
  inputs,
  pkgs,
  lib,
  ...
}:
{
  imports = inputs.self.lib.listModules ./.;

  # I don't use GNU's info
  documentation.info.enable = false;
  environment.systemPackages = [ pkgs.man-pages-posix ];

  # Just expose everything possible so shell completion works
  environment.pathsToLink = [
    "/share/fish"
    "/share/zsh"
    "/share/bash-completion"
  ];

  # Set your time zone.
  time.timeZone = lib.mkDefault "Asia/Ho_Chi_Minh";
}
