{
  inputs,
  pkgs,
  lib,
  ...
}:
{
  # Don't use `auto-optimise-store` Nix config
  # Ref:
  # - https://github.com/LnL7/nix-darwin/pull/1152)
  # - https://github.com/NixOS/nix/issues/7273
  nix.optimise.automatic = true;

  # Add my custom packages
  nixpkgs.overlays = (builtins.attrValues inputs.self.overlays) ++ [
    inputs.neovim-nightly.overlays.default
  ];

  # Reference to nixpkgs inside nixPath and flake registry is already set explicitly
  nixpkgs.flake = {
    setNixPath = false;
    setFlakeRegistry = false;
  };

  nixpkgs.config.allowUnfree = true;

  nix.nixPath = [
    "nixpkgs=${inputs.nixpkgs}"
    "nix-darwin=${inputs.nix-darwin}"
    "home-manager=${inputs.home-manager}"
    "/nix/var/nix/profiles/per-user/root/channels"
  ];

  nix.registry = lib.mapAttrs (_: val: { flake = val; }) (
    lib.filterAttrs (
      name: value: name != "self" && (builtins.hasAttr "_type" value) && value._type == "flake"
    ) inputs
  );

  nix.package = pkgs.nixVersions.latest;

  nix.settings = {
    auto-allocate-uids = true;
    sandbox = true;
    use-xdg-base-directories = true;
    http-connections = 0;
    max-jobs = "auto";

    # build from source if substitutes fail
    fallback = true;

    # for nix-direnv
    keep-outputs = true;
    keep-derivations = true;

    # Fallback quickly if substituters are not available.
    connect-timeout = 5;

    # Avoid copying unnecessary stuff over SSH
    builders-use-substitutes = true;

    # Avoid disk full issues
    max-free = 3072 * 1024 * 1024;
    min-free = 512 * 1024 * 1024;

    # The default at 10 is rarely enough.
    log-lines = 25;

    # Flakes are set explicitly via system's nix.registry, so don't use the global registry
    flake-registry = "";

    # Use this directory on host as bind-mounted /build in the nix-build's sandbox
    # This setting has no effect on MacOS
    build-dir = "/var/tmp";

    # I already have git status information in my shell prompt to tell me this
    warn-dirty = false;

    experimental-features = [
      "auto-allocate-uids"
      "ca-derivations"
      "flakes"
      "nix-command"
      "recursive-nix"
      "dynamic-derivations"
      "git-hashing"
      "fetch-closure"
      # "pipe-operators"
      # "verified-fetches"
    ];

    substituters = [
      "https://nix-community.cachix.org"
    ];
    trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };
}
