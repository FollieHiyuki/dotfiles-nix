# Follow Nix settings for file locations
{ config, lib, ... }:
let
  cfg = config.services.guix;
in
lib.mkMerge [
  {
    services.guix = {
      stateDir = "/gnu/var";
      gc = {
        enable = true;
        extraArgs = [
          "--optimize"
          "--vacuum-database"
          "--delete-generations=14d"
        ];
        dates = lib.mkDefault config.nix.gc.dates;
      };
    };
  }

  (lib.mkIf cfg.enable {
    systemd.services.guix-daemon.environment.TMPDIR = config.nix.settings.build-dir;

    # Allow the usage of `guix system vm`
    # Ref: https://guix.gnu.org/manual/en/html_node/Build-Environment-Setup.html
    users.users =
      lib.genAttrs (builtins.genList (x: "guixbuilder${builtins.toString x}") cfg.nrBuildUsers)
        (_: {
          extraGroups = [ "kvm" ];
        });
  })
]
