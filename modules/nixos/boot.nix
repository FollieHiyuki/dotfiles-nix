{
  pkgs,
  config,
  lib,
  ...
}:
with lib;
mkMerge [
  {
    # Mount tmpfs on /tmp.
    # By default /tmp is used to store nix-build's temporary artifacts, and is managed by systemd tmpfiles.
    # `build-dir` has been set to /var/tmp explicitly for all NixOS hosts, so this is fine.
    boot.tmp.useTmpfs = true;

    boot.plymouth.font = mkDefault "${pkgs.hack-font}/share/fonts/truetype/Hack-Regular.ttf";

    boot.loader = {
      timeout = 10;

      # Rely on NVRAM. Disable this option when an external USB is used for the EFI partition,
      # using boot.loader.grub.efiInstallAsRemovable
      efi.canTouchEfiVariables = mkDefault true;

      grub = {
        inherit (config.boot.plymouth) font;
        efiSupport = true;
        configurationLimit = 10;
        fontSize = mkDefault 24;
      };
    };

    # Some basic kernel hardening
    # FIXME: module signing and LSM lockdown mode can't be enabled
    # Ref: https://github.com/NixOS/nixpkgs/issues/251159
    boot.kernel.sysctl = {
      # There is a security risk of enabling SysRq, but this repo is for desktop machines anyway
      "kernel.sysrq" = 244;

      # Apparently IPv6 privacy extension is only set for "default" interface
      # Ref: https://github.com/NixOS/nixpkgs/issues/250655
      "net.ipv6.conf.all.use_tempaddr" = 2;

      # Allow only privileged users to view dmesg logs
      "kernel.dmesg_restrict" = 1;

      # Disable kexec
      "kernel.kexec_load_disabled" = 1;

      # Hide kernel symbol addresses
      "kernel.kptr_restrict" = 2; # the default is 1

      # Harden BPF
      "net.core.bpf_jit_harden" = 2;
      "kernel.unprivileged_bpf_disabled" = 1; # the default is 2, which can be switched to 0 at runtime
    };
  }

  # What's the point in using Plymouth if it cannot prompt for LUKS password graphically?
  (mkIf config.boot.plymouth.enable {
    boot.initrd.systemd.enable = true;
  })

  (mkIf config.boot.initrd.supportedFilesystems.zfs {
    boot.loader.grub.zfsSupport = true;

    # It's highly recommended to disable this option
    boot.zfs.forceImportRoot = false;
  })
]
