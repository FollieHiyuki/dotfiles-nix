{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
{
  # programs.chromium is re-implemented to separate configuration for brave and chromium.
  # Also simplify to only have `policies` and `enable` options.
  disabledModules = [ "programs/chromium.nix" ];

  options.programs = genAttrs [ "chromium" "brave" ] (item: {
    enable = mkEnableOption "{command}`${item}` policies";

    policies = mkOption {
      type = types.attrs;
      description = ''
        Chromium/Brave policy options.
        See <https://cloud.google.com/docs/chrome-enterprise/policies/>
        and <https://support.brave.com/hc/en-us/articles/360039248271-Group-Policy>.
      '';
      default = { };
    };

    # I don't use these options. They are kept for compatibility with plasma module.
    enablePlasmaBrowserIntegration = mkEnableOption "Native Messaging Host for Plasma Browser Integration";
    plasmaBrowserIntegrationPackage = mkPackageOption pkgs [
      "kdePackages"
      "plasma-browser-integration"
    ] { };
  });

  config = mkMerge [
    (mkIf (config.programs.chromium.enable && config.programs.chromium.policies != { }) {
      environment.etc."chromium/policies/managed/default.json".text =
        builtins.toJSON config.programs.chromium.policies;

      environment.etc."chromium/native-messaging-hosts/org.kde.plasma.browser_integration.json" =
        mkIf config.programs.chromium.enablePlasmaBrowserIntegration
          {
            source = "${config.programs.chromium.plasmaBrowserIntegrationPackage}/etc/chromium/native-messaging-hosts/org.kde.plasma.browser_integration.json";
          };
    })

    (mkIf (config.programs.brave.enable && config.programs.brave.policies != { }) {
      environment.etc."brave/policies/managed/default.json".text =
        builtins.toJSON config.programs.brave.policies;

      environment.etc."brave/native-messaging-hosts/org.kde.plasma.browser_integration.json" =
        mkIf config.programs.brave.enablePlasmaBrowserIntegration
          {
            source = "${config.programs.brave.plasmaBrowserIntegrationPackage}/etc/chromium/native-messaging-hosts/org.kde.plasma.browser_integration.json";
          };
    })

    # Default config
    {
      programs.brave.policies = {
        BraveRewardsDisabled = 1;
        BraveWalletDisabled = 1;
        BraveVPNDisabled = 1;
        BraveAIChatEnabled = 0;
      };
    }
  ];
}
