{
  inputs,
  lib,
  pkgs,
  ...
}:
with lib;
{
  imports = inputs.self.lib.listModules ./.;

  config = {
    environment.systemPackages = [ pkgs.man-pages ];
    documentation = {
      dev.enable = true;
      man.generateCaches = true;
    };

    # There is no reason to stick to X11
    environment.sessionVariables.NIXOS_OZONE_WL = "1";
    xdg.portal.xdgOpenUsePortal = true;

    fonts.fontDir.enable = true;

    # Terminus is a nice console font set
    console.earlySetup = true;
    console.font = lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-h32b.psf.gz";

    # Keep the firmware up-to-date
    services.fwupd.enable = true;

    systemd.enableStrictShellChecks = true;

    hardware.enableRedistributableFirmware = true;

    # Desktop machines need a monitor or two
    hardware.graphics.enable = true;

    hardware.bluetooth.powerOnBoot = false;

    # Set a more restricted umask globally. See pam_umask(8).
    # login.defs file is used here since it has the lowest priority considered by pam_umask
    security.loginDefs.settings.UMASK = "027";
    security.pam.services =
      let
        text = ''
          session optional pam_umask.so
        '';
      in
      {
        login.text = mkDefault text;
        systemd-user.text = mkDefault text;

        # Allow only wheel group to assume another user using `su`
        su.text = mkDefault ''
          auth required pam_wheel.so use_uid
        '';
      };
  };
}
