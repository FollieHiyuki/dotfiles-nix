{ lib, ... }:
{
  # The notion of "online" is a broken concept
  # https://github.com/systemd/systemd/blob/e1b45a756f71deac8c1aa9a008bd0dab47f64777/NEWS#L13
  systemd.services.NetworkManager-wait-online.enable = false;
  systemd.network.wait-online.enable = false;

  services.connman = {
    wifi.backend = "iwd";
    networkInterfaceBlacklist = lib.mkDefault [
      "vmnet"
      "vboxnet"
      "virbr"
      "ifb"
      "veth"
      "docker"
      "lxdbr"
      "incusbr"
    ];
  };

  networking.networkmanager = {
    ethernet.macAddress = "stable";

    wifi = {
      backend = "iwd";
      powersave = true;
      macAddress = "stable-ssid";
    };

    settings = {
      # No connectivity check
      connectivity."enabled" = false;

      # `connection.stable-id` setting controls the result of generated values
      connection = {
        # Privacy settings for IPv6 address generation
        "ipv6.ip6-privacy" = 2;
        "ipv6.addr-gen-mode" = "stable-privacy";

        # Use auto-generated strings as DHCP identifier
        "ipv4.dhcp-client-id" = "stable";
        "ipv4.dhcp-iaid" = "stable";
        "ipv6.dhcp-iaid" = "stable";
        "ipv6.dhcp-duid" = "stable-uuid";
      };
    };
  };
}
