{ pkgs, ... }:
{
  # TODO: add configuration for the input methods
  i18n.inputMethod.fcitx5 = {
    waylandFrontend = true;

    addons = with pkgs; [
      fcitx5-bamboo
      fcitx5-mozc
    ];
  };
}
