# Nixpkgs overlays with NixPak wrapper

This module allows the same overridden packages in the top-level **overlays/** directory to be wrapped again to run under a restricted `bubblewrap` sandbox. It's separated from the general overlays directory, since `bubblewrap` only works on Linux.
