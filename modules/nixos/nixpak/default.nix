# Ref: https://github.com/nixpak/nixpak/issues/87#issuecomment-2409687902
{
  inputs,
  config,
  lib,
  ...
}:
let
  cfg = config.security.nixpak;
in
with lib;
{
  imports = inputs.self.lib.listModules ./.;

  options.security.nixpak = {
    enable = mkEnableOption "nixpak";

    apps = mkOption {
      type = types.anything; # FIXME: get the options from nested submodules somehow
      default = { };
      description = ''
        Applications to be wrapped with nixpak and overlay the originals.
      '';
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      # Use mkAfter so that the overlay is processed last in the list.
      # NixPak completely rewrites packages' derivation interface. As
      # such, no more overlays to the same packages can be applied
      # correctly anymore.
      nixpkgs.overlays = mkAfter [
        (
          final: prev:
          let
            mkNixPak = inputs.nixpak.lib.nixpak {
              inherit lib;
              pkgs = prev;
            };
          in
          builtins.mapAttrs (
            _: nixPakConfig:
            (mkNixPak {
              config = { sloth, ... }@args: nixPakConfig final prev args;
            }).config.env
          ) cfg.apps
        )
      ];
    })

    { security.nixpak.enable = mkDefault true; }
  ];
}
