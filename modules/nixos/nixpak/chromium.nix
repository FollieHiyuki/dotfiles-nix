# Ref:
# - https://github.com/nixpak/pkgs/blob/master/pkgs/browsers/chromium/default.nix
# - https://github.com/flathub/io.github.ungoogled_software.ungoogled_chromium/blob/master/io.github.ungoogled_software.ungoogled_chromium.yaml
let
  appId = "org.chromium.Chromium";
in
_: {
  security.nixpak.apps.ungoogled-chromium =
    _: prev:
    { sloth, ... }:
    {
      app.package = prev.ungoogled-chromium;
      flatpak.appId = appId;

      etc.sslCertificates.enable = true;
      gpu.enable = true;
      fonts.enable = true;
      locale.enable = true;

      dbus.policies = {
        "${appId}" = "own";
        "org.mpris.MediaPlayer2.chromium.*" = "own";
        "org.freedesktop.DBus" = "talk";
        "org.gtk.vfs.*" = "talk";
        "org.gtk.vfs" = "talk";
        "ca.desrt.dconf" = "talk";
        "org.freedesktop.portal.*" = "talk";
        "org.a11y.Bus" = "talk";
      };

      bubblewrap = {
        network = true;
        sockets = {
          wayland = true;
          pulse = true;
          pipewire = true;
        };
        bind.ro = [
          "/etc/chromium"

          (sloth.concat' sloth.runtimeDir "/doc")
          (sloth.concat' sloth.xdgConfigHome "/gtk-2.0")
          (sloth.concat' sloth.xdgConfigHome "/gtk-3.0")
          (sloth.concat' sloth.xdgConfigHome "/gtk-4.0")
          (sloth.concat' sloth.xdgConfigHome "/fontconfig")
        ];
        bind.rw = [
          [
            sloth.appCacheDir
            sloth.xdgCacheHome
          ]
          [
            (sloth.mkdir (sloth.concat' sloth.appCacheDir "/nixpak-app-shared-tmp"))
            "/tmp"
          ]
          (sloth.concat' sloth.xdgCacheHome "/fontconfig")
          (sloth.concat' sloth.xdgCacheHome "/mesa_shader_cache")
          (sloth.concat' sloth.xdgConfigHome "/chromium")
          (sloth.concat' sloth.homeDir "/Downloads")

          (sloth.concat' sloth.runtimeDir "/at-spi/bus")
          (sloth.concat' sloth.runtimeDir "/gvfsd")
        ];
      };
    };
}
