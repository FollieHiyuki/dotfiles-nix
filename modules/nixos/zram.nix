{ lib, ... }:
{
  # 3 devices are plenty
  services.zram-generator.settings =
    lib.genAttrs (builtins.genList (x: "zram${builtins.toString x}") 3)
      (_: {
        zram-size = "min(ram / 2, 4096)";
        compression-algorithm = "zstd";
      });
}
