{ config, lib, ... }:
lib.mkMerge [
  {
    services.dnscrypt-proxy2.upstreamDefaults = false;

    services.resolved = {
      dnssec = "true";
      dnsovertls = "true";
      fallbackDns = lib.mkForce [ ]; # disable compiled-in fallback DNS list

      # Ref: https://dnsprivacy.org/public_resolvers/#dns-over-tls-dot
      extraConfig = lib.mkDefault ''
        DNS=${
          builtins.concatStringsSep " " [
            "2001:67c:930::1#wikimedia-dns.org"
            "185.71.138.138#wikimedia-dns.org"
            "2606:1a40::2#p2.freedns.controld.com"
            "76.76.2.2#p2.freedns.controld.com"
            "2606:1a40:1::2#p2.freedns.controld.com"
            "76.76.10.2#p2.freedns.controld.com"
            "2a0d:2a00:1::2#security-filter-dns.cleanbrowsing.org"
            "185.228.168.9#security-filter-dns.cleanbrowsing.org"
            "2a0d:2a00:2::2#security-filter-dns.cleanbrowsing.org"
            "185.228.169.9#security-filter-dns.cleanbrowsing.org"
          ]
        }
      '';
    };
  }

  (lib.mkIf config.services.resolved.enable {
    networking.dhcpcd.extraConfig = "nohook resolv.conf";
    networking.networkmanager.dns = "systemd-resolved";
    services.connman.extraFlags = [ "--nodnsproxy" ];
  })

  (lib.mkIf config.services.dnscrypt-proxy2.enable {
    networking.resolvconf = {
      enable = true;
      dnsExtensionMechanism = true;
      useLocalResolver = lib.mkDefault true;
    };

    networking.nameservers = lib.mkForce [ ]; # should be managed with resolvconf's extraConfig
    networking.wireless.iwd.settings.Network.NameResolvingService = "resolvconf";
    networking.dhcpcd.extraConfig = "nohook resolv.conf";
    networking.networkmanager.dns = "none";
    services.connman.extraFlags = [ "--nodnsproxy" ];
  })
]
