{ pkgs, lib, ... }:
{
  services.displayManager.sddm = {
    enableHidpi = true;
    wayland.enable = true;
    theme = lib.mkDefault "breeze";
  };

  environment.plasma6.excludePackages = with pkgs.kdePackages; [
    plasma-browser-integration
    kate
    khelpcenter
    konsole
    krdp
    discover
    xwaylandvideobridge
  ];
}
