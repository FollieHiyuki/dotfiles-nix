{ config, lib, ... }:
with lib;
{
  systemd.services.nix-daemon.environment.TMPDIR = config.nix.settings.build-dir;

  # Keep the desktop responsive when switching to a new generation
  # Ref: https://github.com/nix-community/srvos/blob/main/nixos/common/nix.nix
  nix.daemonCPUSchedPolicy = mkDefault "batch";
  nix.daemonIOSchedClass = mkDefault "idle";
  nix.daemonIOSchedPriority = mkDefault 7;

  systemd.services.nix-gc.serviceConfig = {
    CPUSchedulingPolicy = "batch";
    IOSchedulingClass = "idle";
    IOSchedulingPriority = 7;
  };

  # Make builds to be more likely killed than important services.
  # 100 is the default for user slices and 500 is systemd-coredumpd@
  # We rather want a build to be killed than our precious user sessions as builds can be easily restarted.
  systemd.services.nix-daemon.serviceConfig.OOMScoreAdjust = mkDefault 250;

  # cgroups isn't available on MacOS
  nix.settings = {
    trusted-users = [ "@wheel" ];
    use-cgroups = true;
    experimental-features = [ "cgroups" ];
  };

  nix.gc = {
    automatic = true;
    options = "--delete-older-than 14d";
    dates = "weekly";
    persistent = true;
  };
}
