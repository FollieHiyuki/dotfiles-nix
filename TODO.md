# Notes for future improvements

## General

## Darwin

- [ ] amethyst -> yabai / aerospace
  - skhd alternatives: KeyboardCowboy, Hammerspoon (.e.g with PaperWM.spoon)
- [ ] unbound
- [ ] iterm2 module (import from a generated `profile.json` file: Nord theme, Map Ctrl to `+ESC`)
- [ ] privoxy

## NixOS

- [ ] Secure boot (lanzaboote)
- [ ] IOMMU setup with AMD
- [ ] man-db -> mandoc (home-manager doesn't support mandoc for generating index cache yet)
- [ ] privoxy
- [ ] [ananicy-cpp](https://gitlab.com/ananicy-cpp/ananicy-cpp)

## home-manager

- [ ] mpv
- [ ] tmux
- [ ] Use custom RGB colors for ZSH plugins (fast-syntax-highliting / zsh-autosuggestions)
- [ ] contour / ghostty / wezterm terminals
- [ ] gdb (configuration with [GEF](https://github.com/hugsy/gef))
- [ ] emacs -> elfeed-tubes (<https://github.com/karthink/elfeed-tube/pull/39>)
- [ ] plasma-manager
  - Default terminal application (`~/.config/kdeglobals` -> `General > TerminalApplication`)
  - KRunner: center position and disable plugins
