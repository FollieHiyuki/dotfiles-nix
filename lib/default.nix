{ lib, ... }:
let
  modules = lib.mapAttrs (file: _: import ./${file} { inherit lib; }) (
    lib.filterAttrs (name: type: type == "regular" && name != "default.nix") (builtins.readDir ./.)
  );
in
with lib;
foldr (a: b: a // b) { } (attrValues modules)
