_: {
  # This function outputs a bash script to alias Darwin applications to
  # the location indexed by Spotlight.
  # Ref:
  # - https://github.com/LnL7/nix-darwin/issues/214
  # - https://github.com/nix-community/home-manager/issues/1341
  # FIXME: this is a temporary workaround. Remove when fixed upstream
  aliasDarwinApps =
    { apps, targetDir }:
    ''
      if [[ ! -v DRY_RUN ]] && [ -d "${targetDir}" ]; then
        rm -rf "${targetDir}"
      fi
      mkdir -p "${targetDir}"

      find -H -L "${apps}/Applications" -maxdepth 1 -name "*.app" -exec readlink -f '{}' + | while read -r app; do
        target_app="$(basename "$app")"
        echo "Alias $app -> $target_app" >&2

        if [[ ! -v DRY_RUN ]]; then
          /usr/bin/osascript -e "
            tell application \"Finder\"
              make alias file at POSIX file \"${targetDir}\" to POSIX file \"$app\"
              set name of result to \"$target_app\"
            end tell
          " >&2
        fi
      done
    '';
}
