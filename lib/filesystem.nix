{ lib, ... }:
rec {
  # builtins.readDir, but only contains directory entries
  filterDirs = dir: lib.filterAttrs (_: type: type == "directory") (builtins.readDir dir);

  # Return a list everything importable under the specified directory
  listModules =
    dir:
    lib.mapAttrsToList (n: _: dir + "/${n}") (
      lib.filterAttrs (
        name: type:
        (type == "regular" && name != "default.nix" && lib.hasSuffix ".nix" name)
        || (type == "directory" && builtins.pathExists "${builtins.toString dir}/${name}/default.nix")
      ) (builtins.readDir dir)
    );

  # Return a flatten list of all importable targets recursively under
  # the specified directory
  listModulesRecursive =
    dir:
    (listModules dir)
    ++ (lib.flatten (
      lib.mapAttrsToList (name: _: listModulesRecursive (dir + "/${name}")) (filterDirs dir)
    ));

  # A worsen version of the fist function, which only lists sub-directories
  # inside the specified directory with a direct default.nix file inside them
  listModuleDirs =
    dir:
    lib.mapAttrsToList (n: _: dir + "/${n}") (
      lib.filterAttrs (
        name: type:
        type == "directory" && builtins.pathExists "${builtins.toString dir}/${name}/default.nix"
      ) (builtins.readDir dir)
    );

  # Return a list of all sub-directories recursively containing a
  # default.nix file inside the specified directory
  listModuleDirsRecursive =
    dir:
    (listModuleDirs dir)
    ++ (lib.flatten (
      lib.mapAttrsToList (name: _: listModuleDirsRecursive (dir + "/${name}")) (filterDirs dir)
    ));
}
