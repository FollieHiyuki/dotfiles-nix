inputs@{ flake-parts, nixpkgs, ... }:
let
  inherit (nixpkgs) lib;
in
flake-parts.lib.mkFlake { inherit inputs; } {
  systems = [
    "x86_64-linux"
    "aarch64-linux"
    "aarch64-darwin"
  ];

  imports = [
    ./hosts
    ./devShell.nix
    ./treefmt.nix
  ];

  perSystem =
    { system, ... }:
    {
      _module.args = {
        inherit lib;

        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
          overlays = builtins.attrValues inputs.self.overlays;
        };
      };
    };

  flake = {
    # expose for other modules to use
    lib = import ./lib { inherit lib; };

    # custom packages
    overlays =
      with lib;
      mapAttrs' (name: _: nameValuePair (removeSuffix ".nix" name) (import ./overlays/${name})) (
        filterAttrs (name: type: (type == "regular" && hasSuffix ".nix" name)) (builtins.readDir ./overlays)
      );

    # Make sure my custom functions don't do anything stupid
    tests = import ./tests {
      inherit (nixpkgs) lib;
      myLib = inputs.self.lib;
    };
  };
}
