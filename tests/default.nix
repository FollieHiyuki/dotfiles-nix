{ myLib, ... }:
{
  test_filterDirs_1 = {
    expr = myLib.filterDirs ./testdata;
    expected = {
      a = "directory";
      b = "directory";
    };
  };

  test_listModules_1 = {
    expr = myLib.listModules ./testdata;
    expected = [
      ./testdata/b
      ./testdata/c.nix
    ];
  };

  test_listModulesRecursive_1 = {
    expr = myLib.listModulesRecursive ./testdata;
    expected = [
      ./testdata/b
      ./testdata/c.nix
      ./testdata/a/aa
      ./testdata/a/ab.nix
      ./testdata/a/aa/aaa.nix
      ./testdata/b/ba.nix
    ];
  };

  test_listModuleDirs_1 = {
    expr = myLib.listModuleDirs ./testdata;
    expected = [ ./testdata/b ];
  };

  test_listModuleDirsRecursive_1 = {
    expr = myLib.listModuleDirsRecursive ./testdata;
    expected = [
      ./testdata/b
      ./testdata/a/aa
    ];
  };
}
